<?php
/**
 * Created by PhpStorm.
 * User: ANDROMEDA
 * Date: 3/24/2018
 * Time: 2:16 PM
 */

namespace App\Jobs\Reviews;


use App\Jobs\Media\DeleteAllMedia;
use App\Models\Contents\Review;

class DeleteReview
{
    protected $review;

    public function __construct(Review $review)
    {
        $this->review = $review;
    }

    public function handle()
    {
        if ($this->review->media()->count() > 0) {
            dispatch(new DeleteAllMedia($this->review));
        }

        try {
            $delete = $this->review->delete();
        } catch (\Exception $exception) {
            return back()->withErrors([$exception]);
        }

        return $delete;
    }
}