<?php
/**
 * Created by PhpStorm.
 * User: ANDROMEDA
 * Date: 3/24/2018
 * Time: 2:11 PM
 */

namespace App\Jobs\Reviews;

use App\Jobs\Media\CreateMedia;
use App\Models\Contents\Review;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class UpdateReview
{
    protected $request;

    protected $review;

    public function __construct(Request $request, Review $review)
    {
        $this->request = $request;
        $this->review = $review;
    }

    public function handle()
    {
        $validator = Validator::make($this->request->all(), [
            'title' => 'required|string',
            'body' => 'required|string',
            'rating' => 'required|decimal'
        ]);

        if ($validator->fails()) {
            return back()->withErrors([$validator->errors()]);
        }

        try {
            $edit = $this->update();
        } catch (\Exception $exception) {
            return back()->withErrors([$exception]);
        }

        if ($this->request->hasFile('images')) {
            dispatch(new CreateMedia($edit, $this->request));
        }

        return $edit;
    }

    public function update()
    {
        $this->review->update([
            'title' => $this->request->article,
            'body' => $this->request->body,
            'rating' => $this->request->rating,
        ]);

        return $this->review;
    }
}