<?php
/**
 * Created by PhpStorm.
 * User: ANDROMEDA
 * Date: 3/24/2018
 * Time: 2:02 PM
 */

namespace App\Jobs\Reviews;


use App\Jobs\Media\CreateMedia;
use App\Models\Contents\Article;
use App\Models\Contents\Review;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class CreateReview
{
    protected $request;

    protected $article;

    protected $review;

    public function __construct(Request $request, Article $article)
    {
        $this->request = $request;
        $this->article = $article;
        $this->review = new Review();
    }

    public function handle()
    {
        $validator = Validator::make($this->request->all(), [
            'title' => 'required|string',
            'body' => 'required|string',
            'rating' => 'required|decimal'
        ]);

        if ($validator->fails()) {
            return back()->withErrors([$validator->errors()]);
        }

        try {
            $create = $this->save();
        } catch (\Exception $exception) {
            return back()->withErrors([$exception]);
        }

        if ($this->request->hasFile('images')) {
            dispatch(new CreateMedia($create, $this->request));
        }

        return $create;
    }

    protected function save()
    {
        $this->review->fill([
            'user_id' => Auth()->user(),
            'article_id' => $this->article,
            'title' => $this->request->article,
            'body' => $this->request->body,
            'rating' => $this->request->rating,
        ]);

        $this->review->save();

        return $this->review;
    }
}