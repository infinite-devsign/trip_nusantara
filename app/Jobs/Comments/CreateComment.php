<?php
/**
 * Created by PhpStorm.
 * User: ANDROMEDA
 * Date: 3/24/2018
 * Time: 1:42 PM
 */

namespace App\Jobs\Comments;

use App\Models\Contents\Comment;
use App\Models\Contents\Content;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class CreateComment
{
    protected $request;

    protected $content;

    protected $comment;

    public function __construct(Request $request, Content $content)
    {
        $this->request = $request;
        $this->content = $content;
        $this->comment = new Comment();
    }

    /**
     * @return Comment
     */
    public function handle()
    {
        $validator = Validator::make($this->request->all(), [
            'body' => 'required|string',
        ]);

        if ($validator->fails()) {
            return back()->withErrors([$validator->errors()]);
        }

        try {
            $create = $this->save();
        } catch (\Exception $exception) {
            return back()->withErrors([$exception]);
        }

        return $create;
    }

    protected function save()
    {
        $comment = $this->comment->fill([
            'user_id' => Auth()->user(),
            'content_id' => $this->content,
            'body' => $this->request->body,
        ]);
        $comment->save();

        $comment['success'] = 'Comment telah dibuat';

        return $comment;
    }
}