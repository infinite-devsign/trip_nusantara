<?php
/**
 * Created by PhpStorm.
 * User: ANDROMEDA
 * Date: 3/24/2018
 * Time: 1:54 PM
 */

namespace App\Jobs\Comments;


use App\Models\Contents\Comment;

class DeleteComment
{
    protected $comment;

    public function __construct(Comment $comment)
    {
        $this->comment = $comment;
    }

    public function handle()
    {
        try {
            $delete = $this->comment->delete();
        } catch (\Exception $exception) {
            return back()->withErrors([$exception]);
        }

        $this->comment['success'] = 'Komentar telah dihapus';

        return $this->comment;
    }
}