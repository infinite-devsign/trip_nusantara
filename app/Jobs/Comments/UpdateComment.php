<?php
/**
 * Created by PhpStorm.
 * User: ANDROMEDA
 * Date: 3/24/2018
 * Time: 1:49 PM
 */

namespace App\Jobs\Comments;

use App\Models\Contents\Comment;
use App\Models\Contents\Content;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class UpdateComment
{
    protected $request;

    protected $comment;

    public function __construct(Request $request, Comment $comment)
    {
        $this->request = $request;
        $this->comment = $comment;
    }

    public function handle()
    {
        $validator = Validator::make($this->request->all(), [
            'body' => 'required|string',
        ]);

        if ($validator->fails()) {
            return back()->withErrors([$validator->errors()]);
        }

        try {
            $edit = $this->update();
        } catch (\Exception $exception) {
            return back()->withErrors([$exception]);
        }

        return $edit;
    }

    protected function update()
    {
        $this->comment->update([
            'body' => $this->request->body,
        ]);

        $this->comment['success'] = 'Comment telah diperbarui';

        return $this->comment;
    }
}