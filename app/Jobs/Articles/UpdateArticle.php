<?php
/**
 * Created by PhpStorm.
 * User: ANDROMEDA
 * Date: 3/21/2018
 * Time: 9:47 PM
 */

namespace App\Jobs\Articles;

use App\Models\Contents\Article;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class UpdateArticle
{
    protected $request;

    protected $article;

    public function __construct(Request $request, Article $article)
    {
        $this->request = $request;
        $this->article = $article;
    }

    public function handle()
    {
        $validator = Validator::make($this->request->all(), [
            'title' => 'required',
            'description' => 'required',
            'location' => 'required',
        ]);

        if ($validator->fails()) {
            return back()->withErrors([$validator->errors()]);
        }

        try {
            $this->article->title = $this->request->title;
            $this->article->description = $this->request->description;
            $this->article->location = $this->request->location;
            $this->article->geo_location = $this->getGeoCode();

            $this->article->save();
        } catch (\Exception $exception) {
            return redirect()->back()->withErrors(['error' => $exception]);
        }

        return $this->article;
    }

    protected function getGeoCode()
    {
        $url = "https://maps.google.com/maps/api/geocode/json?address=" . urlencode($this->request->location) . "&sensor=false&region=indonesia&key=AIzaSyDJbfXmvMlJoEvHbIMdiHwCWb5yXaHHH2c";
        $json = file_get_contents($url);
        $data = json_decode($json);

        foreach ($data->results as $datum) {
            $lat = $datum->geometry->location->lat;
            $lng = $datum->geometry->location->lng;

            $array = [$lat, $lng];
        }

        $geo = implode(', ', $array);

        return $geo;
    }
}