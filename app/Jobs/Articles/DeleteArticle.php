<?php
/**
 * Created by PhpStorm.
 * User: ANDROMEDA
 * Date: 3/21/2018
 * Time: 9:52 PM
 */

namespace App\Jobs\Articles;

use App\Jobs\Media\DeleteAllMedia;
use App\Models\Contents\Article;

class DeleteArticle
{
    protected $article;

    public function __construct(Article $article)
    {
        $this->article = $article;
    }

    public function handle()
    {
        if ($this->article->media()->count() != 0) {
            dispatch(new DeleteAllMedia($this->article));
        }

        try {
            $this->article->delete();
        } catch (\Exception $exception) {
            return back()->withErrors([$exception]);
        }

        return $this->article;
    }
}