<?php
/**
 * Created by PhpStorm.
 * User: ANDROMEDA
 * Date: 3/21/2018
 * Time: 8:34 PM
 */

namespace App\Jobs\Articles;

use App\Jobs\Media\CreateMedia;
use App\Models\Contents\Article;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CreateArticle
{
    protected $request;

    protected $article;

    public function __construct(Request $request)
    {
        $this->request = $request;
        $this->article = new Article();
    }

    public function handle()
    {
        $this->article->fill([
            'user_id' => Auth()->user()->id,
            'title' => $this->request->title,
            'slug' => $this->getSlugByTitle(),
            'description' => $this->request->description,
            'location' => $this->request->location,
            'geo_location' => $this->getGeoCode(),
            'type' => $this->request->type,
        ]);

        $this->article->save();

        if ($this->request->hasFile('images')) {
            dispatch(new CreateMedia($this->article, $this->request));
        }

        return $this->article;
    }

    protected function getSlugByTitle()
    {
        $str = strtolower($this->request->title);
        return str_slug($str, '-') . '-' . hash('crc32b', uniqid()) . '.html';
    }

    protected function getGeoCode()
    {
        $url = "https://maps.google.com/maps/api/geocode/json?address=" . urlencode($this->request->location) . "&sensor=false&region=indonesia&key=AIzaSyDJbfXmvMlJoEvHbIMdiHwCWb5yXaHHH2c";
        $json = file_get_contents($url);
        $data = json_decode($json);

        foreach ($data->results as $datum) {
            $lat = $datum->geometry->location->lat;
            $lng = $datum->geometry->location->lng;

            $array = [$lat, $lng];
        }

        $geo = implode(', ', $array);

        return $geo;
    }
}