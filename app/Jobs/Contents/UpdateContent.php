<?php
/**
 * Created by PhpStorm.
 * User: ANDROMEDA
 * Date: 3/20/2018
 * Time: 1:03 PM
 */

namespace App\Jobs\Contents;

use App\Models\Contents\Content;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class UpdateContent
{
    protected $content;

    protected $request;

    public function __construct(Request $request, Content $content)
    {
        $this->content = $content;
        $this->request = $request;
    }

    public function handle()
    {


        $this->content->update([
            'user_id' => $this->content->user_id,
            'title' => $this->request->title,
            'description' => $this->request->description,
            'date_start' => $this->getDateStart(),
        ]);

        return $this->content;
    }

    protected function getDateStart()
    {
        if ($this->request->has('date_start')) {
            $str = implode('-' ,$this->request->date_start);
            return $str;
        } else {
            return null;
        }
    }
}