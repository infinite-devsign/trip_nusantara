<?php
/**
 * Created by PhpStorm.
 * User: ANDROMEDA
 * Date: 3/21/2018
 * Time: 8:10 PM
 */

namespace App\Jobs\Contents;

use App\Jobs\Media\DeleteAllMedia;
use App\Models\Contents\Content;

class DeleteContent
{
    protected $content;

    public function __construct(Content $content)
    {
        $this->content = $content;
    }

    public function handle()
    {
        try {
            $this->content->delete();
        } catch (\Exception $exception) {
            return back()->withErrors([$exception]);
        }

        dispatch(new DeleteAllMedia($this->content));

        return $this->content;
    }
}