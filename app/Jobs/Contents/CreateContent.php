<?php
/**
 * Created by PhpStorm.
 * User: ANDROMEDA
 * Date: 3/19/2018
 * Time: 8:34 PM
 */

namespace App\Jobs\Contents;

use App\Jobs\Media\CreateMedia;
use App\Models\Contents\Content;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CreateContent
{
    protected $request;

    protected $content;

    public function __construct(Request $request)
    {
        $this->request = $request;
        $this->content  = new Content();
    }

    /**
     * @return $this|Content
     */
    public function handle()
    {
        $this->content->fill([
            'user_id' => Auth()->user()->id,
            'title' => $this->request->title,
            'slug' => $this->getSlugByTitle(),
            'description' => $this->request->description,
            'date_start' => $this->getDateStart(),
            'type' => $this->request->type,
        ]);

        $this->content->save();

        if ($this->request->hasFile('images'))
            dispatch(new CreateMedia($this->content, $this->request));

        return $this->content;
    }

    protected function getDateStart()
    {
        if ($this->request->has('date_start')) {
            $str = implode('-' ,$this->request->date_start);
            return $str;
        } else {
            return null;
        }
    }

    protected function getSlugByTitle()
    {
        $str = strtolower($this->request->title);
        return str_slug($str, '-').'-'.hash('crc32b', uniqid()).'.html';
    }
}