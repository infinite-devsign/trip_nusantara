<?php

namespace App\Jobs\Emails;

use App\Mail\PasswordReset;
use App\Models\Accounts\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Mail;

class SendPasswordReset implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $user;

    /**
     * Create a new job instance.
     *
     * @param User $user
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * Execute the job.
     *
     * @return User|\Exception
     */
    public function handle()
    {
        try {
            $email = new PasswordReset($this->user);
            Mail::to($this->user->email)->send($email);
            return $this->user;
        } catch (\Exception $e) {
            dd($e);
        }
    }
}
