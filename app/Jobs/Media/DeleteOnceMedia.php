<?php
/**
 * Created by PhpStorm.
 * User: ANDROMEDA
 * Date: 3/21/2018
 * Time: 8:25 PM
 */

namespace App\Jobs\Media;

use App\Core\Contracts\Databases\ContentTableInterface;
use App\Models\Contents\Media;

class DeleteOnceMedia
{
    protected $model;

    protected $media;

    public function __construct(ContentTableInterface $model,Media $media)
    {
        $this->model = $model;
        $this->media = $media;
    }

    public function handle()
    {
        try {
            $this->model->media()->detach($this->media);

            $file = app()->make(Filesystem::class);
            $file->delete(storage_path('app/public'.$this->media->src));

            $this->media->delete();
        } catch (\Exception $exception) {
            return back()->withErrors([$exception]);
        }

        return $this->model;
    }
}