<?php
/**
 * Created by PhpStorm.
 * User: ANDROMEDA
 * Date: 3/21/2018
 * Time: 8:12 PM
 */

namespace App\Jobs\Media;


use App\Core\Contracts\Databases\ContentTableInterface;
use App\Models\Contents\Media;
use Illuminate\Filesystem\Filesystem;

class DeleteAllMedia
{
    protected $model;

    public function __construct(ContentTableInterface $model)
    {
        $this->model = $model;
    }

    public function handle()
    {
        try {
            $this->deleteMedia();
        } catch (\Exception $exception) {
            return back()->withErrors([$exception]);
        }

        return $this->model;
    }

    protected function deleteMedia()
    {
        foreach ($this->model as $item) {
            $media = Media::find($item->id);

            $detach = $this->model->media()->find($media->id);
            $detach->delete();

            $file = app()->make(Filesystem::class);
            $file->delete(storage_path('app/public'.$media->src));

            $media->delete();
        }

        return $this->model;
    }
}