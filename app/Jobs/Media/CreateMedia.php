<?php
/**
 * Created by PhpStorm.
 * User: ANDROMEDA
 * Date: 3/19/2018
 * Time: 8:50 PM
 */

namespace App\Jobs\Media;

use App\Core\Contracts\Databases\ContentTableInterface;
use App\Models\Contents\Media;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

class CreateMedia
{
    protected $model;

    protected $request;

    public function __construct(ContentTableInterface $model, Request $request)
    {
        $this->model = $model;
        $this->request = $request;
    }

    public function handle()
    {
        try {
            $media = $this->model->media()->saveMany($this->uploadFiles());
        } catch (\Exception $exception) {
            return response()->json([$exception]);
        }

        return $media;
    }

    protected function uploadFiles()
    {
        $uploaded = [];

        if ($this->request->hasFile('images')) {
            foreach ($this->request->images as $image) {
                $folder = $this->getUploadDirectory();
                $realImage = 'image-' . hash('crc32b', uniqid()) . '.' . $image->getClientOriginalExtension();
                $path = $folder . '/' . $realImage;

                $thumb = Image::make($image->getRealPath());
                $thumb->fit(800, 600, function ($constraint) {
                    $constraint->upsize();
                });
                Storage::put('public' . $path, (string)$thumb->encode());

                array_push($uploaded, new Media([
                    'slug' => $this->getSlugByNameAndExt($image),
                    'src' => $path,
                    'type' => 'image',
                ]));
            }
        }

        if ($this->request->hasFile('cover')) {
            foreach ($this->request->cover as $cover) {
                $folder = $this->getUploadDirectory();
                $realImage = 'cover-' . hash('crc32b', uniqid()) . '.' . $cover->getClientOriginalExtension();
                $path = $folder . '/' . $realImage;

                $thumb = Image::make($cover->getRealPath());
                $thumb->fit(1200, 400);

                Storage::put('public' . $path, (string)$thumb->encode());

                array_push($uploaded, new Media([
                    'slug' => $this->getSlugByNameAndExt($cover),
                    'src' => $path,
                    'type' => 'cover',
                ]));
            }
        }

        if ($this->request->hasFile('avatar')) {
            foreach ($this->request->avatar as $avatar) {
                $folder = $this->getUploadDirectory();
                $realImage = 'avatar-' . hash('crc32b', uniqid()) . '.' . $avatar->getClientOriginalExtension();
                $path = $folder . '/' . $realImage;

                $thumb = Image::make($avatar->getRealPath());
                $thumb->fit(400);

                Storage::put('public' . $path, (string)$thumb->encode());

                array_push($uploaded, new Media([
                    'slug' => $this->getSlugByNameAndExt($avatar),
                    'src' => $path,
                    'type' => 'avatar',
                ]));
            }
        }

        return $uploaded;
    }

    protected function getUploadDirectory()
    {
        return '/' . substr(hash('crc32b', uniqid()), 0, 2) . '/' . substr(hash('crc32b', uniqid()), -2);
    }

    protected function getSlugByNameAndExt($image)
    {
        $str = md5($image->getClientOriginalName()) . Carbon::now() . '-'
            . $image->getClientOriginalExtension();

        return str_slug($str);
    }
}