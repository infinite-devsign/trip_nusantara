<?php

namespace App\Models\Accounts;

use App\Core\Contracts\Databases\ContentTableInterface;
use App\Models\Contents\Article;
use App\Models\Contents\Comment;
use App\Models\Contents\Content;
use App\Models\Contents\Media;
use App\Models\Contents\Review;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

/**
 * @property mixed verification_token
 */
class User extends Authenticatable implements ContentTableInterface
{
    use Notifiable;

    const ADMIN = "admin";

    const MEMBER = "member";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'username', 'email', 'password', 'role', 'status', 'token'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function setPasswordAttribute($password)
    {
        $this->attributes['password'] = bcrypt($password);
    }

    public function contents()
    {
        return $this->hasMany(Content::class, 'user_id', 'id');
    }

    public function articles()
    {
        return $this->hasMany(Article::class, 'user_id', 'id');
    }

    public function media()
    {
        return $this->morphToMany(Media::class, 'mediable', 'mediables');
    }

    public function comments()
    {
        return $this->hasMany(Comment::class, 'user_id', 'id');
    }

    public function reviews()
    {
        return $this->hasMany(Review::class, 'user_id', 'id');
    }

    /**
     * Check role is member or not
     *
     * @return boolean
     */
    public function isMember ()
    {
        return $this->attributes['role'] == self::MEMBER ? true : false;
    }

    /**
     * Check role is admin or not
     *
     * @return bool
     */
    public function isAdmin ()
    {
        return $this->attributes['role'] == self::ADMIN ? true : false;
    }
}
