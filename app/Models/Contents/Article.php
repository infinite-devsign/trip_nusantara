<?php
/**
 * Created by PhpStorm.
 * User: ANDROMEDA
 * Date: 3/19/2018
 * Time: 8:25 PM
 */

namespace App\Models\Contents;

use App\Core\Contracts\Databases\ContentTableInterface;
use App\Models\Accounts\User;
use Illuminate\Database\Eloquent\Model;

/**
 * @property mixed $media
 */
class Article extends Model implements ContentTableInterface
{
    protected $table = 'articles';

    protected $fillable = [
        'user_id', 'title', 'slug', 'description', 'location',
        'geo_location', 'type', 'status'
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    /**
     * @return \App\Core\Contracts\Databases\Illuminate\Database\Eloquent\Relations\MorphToMany|\Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function media()
    {
        return $this->morphToMany(Media::class, 'mediable', 'mediables');
    }

    public function reviews()
    {
        return $this->hasMany(Review::class, 'article_id', 'id');
    }
}