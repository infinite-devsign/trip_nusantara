<?php

namespace App\Models\Contents;

use App\Core\Contracts\Databases\ContentTableInterface;
use App\Core\Contracts\Databases\Illuminate;
use App\Models\Accounts\User;
use Illuminate\Database\Eloquent\Model;

class Review extends Model implements ContentTableInterface
{
    protected $table = 'reviews';

    protected $fillable = [
        'user_id', 'article_id', 'title', 'body', 'rating',
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function article()
    {
        return $this->belongsTo(Article::class, 'article_id', 'id');
    }

    /**
     * get all of media for the contents and users
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphToMany
     */
    public function media()
    {
        return $this->morphToMany(Media::class, 'mediable', 'mediables');
    }
}
