<?php

namespace App\Models\Contents;

use App\Models\Accounts\User;
use Illuminate\Database\Eloquent\Model;

class Media extends Model
{
    protected $table = 'media';

    protected $fillable = [
        'slug', 'src', 'type',
    ];

    public function users()
    {
        return $this->morphedByMany(User::class, 'mediable', 'mediables');
    }

    public function contents()
    {
        return $this->morphedByMany(Content::class, 'mediable', 'mediables');
    }
}
