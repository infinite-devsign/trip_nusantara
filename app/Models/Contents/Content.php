<?php

namespace App\Models\Contents;

use App\Core\Contracts\Databases\ContentTableInterface;
use App\Models\Accounts\User;
use Illuminate\Database\Eloquent\Model;

class Content extends Model implements ContentTableInterface
{
    protected $table = 'contents';
    
    protected $fillable = [
        'user_id', 'title', 'slug', 'description', 'date_start', 'type', 'status'
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function media()
    {
        return $this->morphToMany(Media::class, 'mediable', 'mediables');
    }

    public function comments()
    {
        return $this->hasMany(Comment::class, 'content_id', 'id');
    }
}
