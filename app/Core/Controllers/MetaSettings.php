<?php
/**
 * Created by PhpStorm.
 * User: ANDROMEDA
 * Date: 4/10/2018
 * Time: 5:55 AM
 */

namespace App\Core\Controllers;

use Illuminate\Support\Facades\View;

trait MetaSettings
{
    protected $pageMeta = [
        'title' => 'title',
        'description' => 'description',
        'keywords' => 'keywords',
        'image' => 'image',
    ];

    public function setPageMeta($metaKey, $metaValue)
    {
        View::share($metaKey, $metaValue);

        return $this;
    }
}