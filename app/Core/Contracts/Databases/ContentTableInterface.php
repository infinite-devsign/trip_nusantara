<?php
/**
 * Created by PhpStorm.
 * User: ANDROMEDA
 * Date: 3/6/2018
 * Time: 3:00 AM
 */

namespace App\Core\Contracts\Databases;

interface ContentTableInterface
{
    /**
     * get all of media for the contents and users
     *
     * @return Illuminate\Database\Eloquent\Relations\MorphToMany
     */
    public function media();
}