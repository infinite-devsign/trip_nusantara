<?php
/**
 * Created by PhpStorm.
 * User: ANDROMEDA
 * Date: 4/19/2018
 * Time: 9:15 PM
 */

namespace App\Http\Controllers\Guest;


use App\Http\Controllers\Controller;
use App\Models\Contents\Content;

class CulinaryController extends Controller
{
    protected $type = 'culinary';

    public function index()
    {
        $culinary = Content::where('type', '=', $this->type)->get();

        $data = [
            'culinary' => $culinary
        ];

        return view('culinary.index', $data);
    }

    public function show($culinary)
    {
        try {
            $culinary = Content::where('type', '=', $this->type)->where('slug', '=', $culinary)->first();
        } catch (\Exception $exception) {
            return back()->withErrors('Destinasi tidak ada
            ');
        }

        $data = [
            'culinary' => $culinary,
        ];

        return view('culinary.show', $data);
    }
}