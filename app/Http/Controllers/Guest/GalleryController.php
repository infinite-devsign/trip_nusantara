<?php
/**
 * Created by PhpStorm.
 * User: ANDROMEDA
 * Date: 4/19/2018
 * Time: 10:05 PM
 */

namespace App\Http\Controllers\Guest;


use App\Http\Controllers\Controller;
use App\Models\Contents\Content;

class GalleryController extends Controller
{
    protected $type = 'gallery';

    public function index()
    {
        $culinary = Content::where('type', '=', $this->type)->get();

        $data = [
            'gallery' => $culinary
        ];

        return view('gallery.index', $data);
    }

    public function show($gallery)
    {
        try {
            $destination = Content::where('type', '=', $this->type)->where('slug', '=', $gallery)->first();
        } catch (\Exception $exception) {
            return back()->withErrors('Destinasi tidak ada
            ');
        }

        $data = [
            'gallery' => $destination,
        ];

        return view('gallery.show', $data);
    }
}