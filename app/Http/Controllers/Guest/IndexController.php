<?php

namespace App\Http\Controllers\Guest;

use App\Jobs\Articles\CreateArticle;
use App\Models\Contents\Article;
use App\Http\Controllers\Controller;
use App\Models\Contents\Content;

class IndexController extends Controller
{
    public function index()
    {
        $destination = Article::where('type', '=', 'destination')->limit(4)->get();
        $gallery = Content::where('type', '=', 'gallery')->limit(5)->get();

        $data = [
            'destination' => $destination,
            'gallery' => $gallery
        ];

        return view('welcome', $data);
    }
}
