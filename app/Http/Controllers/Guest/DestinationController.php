<?php
/**
 * Created by PhpStorm.
 * User: ANDROMEDA
 * Date: 4/19/2018
 * Time: 7:25 PM
 */

namespace App\Http\Controllers\Guest;

use App\Http\Controllers\Controller;
use App\Models\Contents\Article;

class DestinationController extends Controller
{
    protected $type = 'destination';

    public function index()
    {
        $culinary = Article::where('type', '=', $this->type)->get();

        $data = [
            'destination' => $culinary
        ];

        return view('destination.index', $data);
    }

    public function show($destination)
    {
        try {
            $destination = Article::where('type', '=', $this->type)->where('slug', '=', $destination)->first();
        } catch (\Exception $exception) {
            return back()->withErrors('Destinasi tidak ada
            ');
        }

        $geo = explode(',', $destination->geo_location);

        $data = [
            'destination' => $destination,
            'position' => $geo,
        ];

        return view('destination.show', $data);
    }
}