<?php

namespace App\Http\Controllers\Auth;

use App\Core\Controllers\MetaSettings;
use App\Models\Accounts\User;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AdminController extends Controller
{
    use AuthenticatesUsers {
        redirectPath as laravelRedirectPath;
    }

    protected $redirectTo = '/admin/';

    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    protected function showLoginForm()
    {
        $this->setPageMeta($this->pageMeta['keywords'], 'Admin Login');

        return view('auth.adminlogin');
    }

    protected function credentials(Request $request)
    {
        $field = filter_var($request->get($this->username()), FILTER_VALIDATE_EMAIL)
            ? $this->username()
            : 'username';

        return [
            $field => $request->get($this->username()),
            'password' => $request->password,
        ];
    }

    public function redirectPath()
    {
        session()->flash('status', 'Selamat datang '.Auth()->user()->name);

        return $this->laravelRedirectPath();
    }
}
