<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Jobs\Emails\SendPasswordReset;
use App\Models\Accounts\User;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Password;

class ForgotPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */

    use SendsPasswordResetEmails;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * @param Request $request
     */
    public function sendResetLinkEmail(Request $request)
    {
        $this->validateEmail($request);

        $user = User::where('email', '=', $request->email)->first();

        if ($user instanceof User) {
            $token = $user->update(['token' => $this->createToken()]);

            try {
                $response = $this->dispatch(new SendPasswordReset($user));
            } catch (\Exception $exception) {
                return back()->with(['status' => 'error']);
            }

            return back()->with(['status' => 'error']);
        }

        return back()->with(['status' => 'email not found']);
    }

    protected function createToken()
    {
        $str = str_random('15');
        $hash = base64_encode($str);

        return $hash;
    }
}
