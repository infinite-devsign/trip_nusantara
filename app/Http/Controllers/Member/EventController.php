<?php

namespace App\Http\Controllers\Member;

use App\Jobs\Contents\CreateContent;
use App\Jobs\Contents\DeleteContent;
use App\Jobs\Contents\UpdateContent;
use App\Models\Contents\Content;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class EventController extends Controller
{
    protected $type = 'event';
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $events = Content::where('type', '=', $this->type)->where('user_id', '=', Auth()->user()->id)->paginate(10);

        $data = [
            'event' => $events
        ];

        return view('admin.events.index', $data);
    }

    /**
     * Show the form for creating a new
     * resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.events.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'title' => 'required',
            'description' => 'required',
            'images.*' => 'required|image|mimes:jpeg,png,jpg,gif,svg',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors());
        }

        $request->type = $this->type;

        try {
            $this->dispatch(new CreateContent($request));
        } catch (\Exception $exception) {
            return redirect()->back()->withErrors($exception);
        }

        return redirect()->route('admin.event')->with('success', 'Event berhasil dibuat');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param $slug
     * @return void
     */
    public function edit($slug)
    {
        $event = Content::where('type', '=', $this->type)
            ->where('slug', '=', $slug)->first();

        if ($event->count() == 0) {
            return redirect()->back()->withErrors('Event tidak ada');
        }

        $date_start = explode('-', $event->date_start);

        $data = [
            'event' => $event,
            'date' => $date_start,
        ];

        return view('admin.events.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param $slug
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $slug)
    {
        $event = Content::where('type', '=', $this->type)
            ->where('slug', '=', $slug)->first();

        if ($event->count() == 0) {
            return redirect()->back()->withErrors('Event tidak ada');
        }

        $validator = Validator::make($request->all(), [
            'title' => 'required',
            'description' => 'required',
            'date_start.*' => 'required|string',
        ]);

        if ($validator->fails()) {
            return back()->with($validator->errors());
        }

        try {
            $this->dispatch(new UpdateContent($request, $event));
        } catch (\Exception $exception) {
            return redirect()->back()->withErrors('Event not found');
        }

        return redirect()->route('admin.event')->with('success', 'Berhasil di update');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($slug)
    {
        $event = Content::where('type', '=', $this->type)
            ->where('slug', '=', $slug)->first();

        if ($event->count() == 0) {
            return redirect()->back()->withErrors('Event tidak ada');
        }

        try {
            $this->dispatch(new DeleteContent($event));
        } catch (\Exception $exception) {
            return redirect()->back()->withErrors('Event tidak ada');
        }

        return redirect()->route('admin.event')->with('success' ,'Berhasil di hapus');
    }
}