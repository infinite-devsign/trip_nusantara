<?php

namespace App\Http\Controllers\Member;

use App\Http\Controllers\Controller;
use App\Models\Contents\Article;
use App\Models\Contents\Content;

class DashboardController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth.check:member');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $destination = Article::where('type', '=', 'destination')
            ->where('user_id', '=', Auth()->user()->id)->count();
        $culinary = Content::where('type', '=', 'culinary')
            ->where('user_id', '=', Auth()->user()->id)->count();
        $gallery = Content::where('type', '=', 'gallery')
            ->where('user_id', '=', Auth()->user()->id)->count();
        $event = Content::where('type', '=', 'event')
            ->where('user_id', '=', Auth()->user()->id)->count();

        $data = [
            'destination' => $destination,
            'culinary' => $culinary,
            'gallery' => $gallery,
            'event' => $event,
        ];

        return view('member.index', $data);
    }
}
