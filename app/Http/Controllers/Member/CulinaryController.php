<?php

namespace App\Http\Controllers\Member;

use App\Http\Controllers\Controller;
use App\Jobs\Contents\CreateContent;
use App\Jobs\Contents\DeleteContent;
use App\Jobs\Contents\UpdateContent;
use App\Models\Contents\Content;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CulinaryController extends Controller
{
    protected $type = 'culinary';

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $culinary = Content::where('type', '=', $this->type)->where('user_id', '=', Auth()->user()->id)->paginate(10);

        $data = [
            'culinary' => $culinary,
        ];

        return view('admin.culinary.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.culinary.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'title' => 'required',
            'description' => 'required',
            'images.*' => 'required|image|mimes:jpeg,png,jpg,gif,svg',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors());
        }

        $request->type = $this->type;

        try {
            $this->dispatch(new CreateContent($request));
        } catch (\Exception $exception) {
            return redirect()->back()->withErrors($exception);
        }

        return redirect()->route()->with('success', 'Kuliner telah dibuat');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($slug)
    {
        $culinary = Content::where('type', '=', $this->type)
            ->where('slug', '=', $slug)->first();

        if ($culinary->count() == 0) {
            return redirect()->back()->withErrors(['error' => 'Kuliner not found']);
        }

        $data = [
            'culinary' => $culinary,
        ];

        return view('admin.culinary.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $slug)
    {
        $culinary = Content::where('type', '=', $this->type)
            ->where('slug', '=', $slug)->first();

        if ($culinary->count() == 0) {
            return redirect()->back()->withErrors('Kuliner tidak ada');
        }

        $validator = Validator::make($request->all(), [
            'title' => 'required',
            'description' => 'required',
            'date_start.*' => 'required|string',
        ]);

        if ($validator->fails()) {
            return back()->with($validator->errors());
        }

        try {
            $this->dispatch(new UpdateContent($request, $culinary));
        } catch (\Exception $exception) {
            return redirect()->back()->withErrors($exception);
        }

        return redirect()->route()->with('success', 'Kuliner telah diedit');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($slug)
    {
        $culinary = Content::where('type', '=', $this->type)
            ->where('slug', '=', $slug)->first();

        if ($culinary->count() == 0) {
            return redirect()->back()->withErrors('Kuliner tidak ada');
        }

        try {
            $this->dispatch(new DeleteContent($culinary));
        } catch (\Exception $exception) {
            return redirect()->back()->withErrors($exception);
        }

        return redirect()->route()->with('success', 'Kuliner telah di delete');
    }
}
