<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Jobs\Contents\CreateContent;
use App\Jobs\Contents\DeleteContent;
use App\Jobs\Contents\UpdateContent;
use App\Models\Contents\Content;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class GalleryController extends Controller
{
    protected $type = 'gallery';

    public function __construct()
    {
        $this->middleware('auth.check:admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $gallery = Content::where('type', '=', $this->type)->paginate(10);

        $data = [
            'gallery' => $gallery
        ];

        return view('admin.gallery.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.gallery.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'title' => 'required',
            'description' => 'required',
            'images.*' => 'required|image|mimes:jpeg,png,jpg,gif,svg',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors());
        }

        $request->type = $this->type;

        try {
            $create = $this->dispatch(new CreateContent($request));
        } catch (\Exception $exception) {
            return redirect()->back()->withErrors($exception);
        }

        return redirect()->route('admin.galeri')->with('success', 'Galeri berhasil dibuat');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($slug)
    {
        $gallery = Content::where('type', '=', $this->type)
            ->where('slug', '=', $slug)->first();

        if ($gallery->count() == 0) {
            return redirect()->route('admin.galeri')->withErrors(['status' => 'Galeri tidak ada']);
        }

        $data = [
            'gallery' => $gallery
        ];

        return view('admin.gallery.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $slug)
    {
        $gallery = Content::where('type', '=', $this->type)
            ->where('slug', '=', $slug)->first();

        if ($gallery->count() == 0) {
            return redirect()->route('admin.galeri')->withErrors('Galeri tidak ada');
        }

        $validator = Validator::make($request->all(), [
            'title' => 'required',
            'description' => 'required',
            'date_start.*' => 'required|string',
        ]);

        if ($validator->fails()) {
            return back()->with($validator->errors());
        }

        try {
            $this->dispatch(new UpdateContent($request, $gallery));
        } catch (\Exception $exception) {
            return redirect()->back()->withErrors($exception);
        }

        return redirect()->route('admin.galeri')->with('success', 'Galeri berhasil di edit');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($slug)
    {
        $gallery = Content::where('type', '=', $this->type)
            ->where('slug', '=', $slug)->first();

        if ($gallery->count() == 0) {
            return redirect()->route('admin.galeri')->withErrors('Galeri tidak ada');
        }

        try {
            $this->dispatch(new DeleteContent($gallery));
        } catch (\Exception $exception) {
            return redirect()->back()->withErrors($exception);
        }

        return redirect()->route('admin.galeri')->with('success', $gallery->title . ' berhasil dihapus');
    }

    public function status($slug, Request $request)
    {
        $gallery = Content::where('type', '=', $this->type)
            ->where('slug', '=', $slug)->first();

        try {
            $gallery->status = $request->status;
            $gallery->save();
        } catch (\Exception $exception) {
            return back()->withErrors(['error' => $exception]);
        }

        return redirect()->route('admin.galeri')->with('success', 'Status telah diperbarui');
    }
}
