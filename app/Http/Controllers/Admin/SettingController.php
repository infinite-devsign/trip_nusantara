<?php
/**
 * Created by PhpStorm.
 * User: ANDROMEDA
 * Date: 4/17/2018
 * Time: 7:18 PM
 */

namespace App\Http\Controllers\Admin;


use App\Http\Controllers\Controller;
use App\Jobs\Media\CreateMedia;
use App\Jobs\Media\DeleteAllMedia;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class SettingController extends Controller
{
    public function index()
    {
        $user = Auth()->user();

        $data = [
            'user' => $user
        ];

        return view('admin.settings', $data);
    }

    public function update(Request $request)
    {
        $user = Auth()->user();

        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'username' => 'required',
            'email' => 'required|string|email',
            'password' => 'required'
        ]);

        if ($validator->fails() == true) {
            return redirect()->back()->withErrors($validator->errors());
        }

        if (password_verify($request->password, $user->password)) {
            try {
                $user->name = $request->name;
                $user->username = $request->username;
                $user->email = $request->email;

                $user->save();
            } catch (\Exception $exception) {
                return redirect()->back()->withErrors('Something errors');
            }

            return redirect()->route('admin.setting')->with('success', 'Data berhasil di update');
        } else {
            return redirect()->back()->withErrors('Password tidak sama');
        }
    }

    public function updateAvatar(Request $request)
    {
        $user = Auth()->user();

        $validator = Validator::make($request->all(), [
            'avatar.*' => 'required|image|mimes:jpeg,png,jpg,gif,svg',
        ]);

        if ($validator->fails() == true) {
            return redirect()->back()->withErrors($validator->errors());
        }

        try {
            $this->dispatch(new DeleteAllMedia($user));
            $this->dispatch(new CreateMedia($user, $request));
        } catch (\Exception $exception) {
            return redirect()->back()->withErrors('Something errors');
        }

        return redirect()->route('admin.setting')->with('success', 'Avatar berhasil di update');
    }
}