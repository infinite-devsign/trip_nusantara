<?php

namespace App\Http\Controllers\Admin;

use App\Models\Contents\Article;
use App\Models\Contents\Content;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DashboardController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth.check:admin');
    }

    public function index()
    {
        $destination = Article::where('type', '=', 'destination')->count();
        $culinary = Content::where('type', '=', 'culinary')->count();
        $gallery = Content::where('type', '=', 'gallery')->count();
        $event = Content::where('type', '=', 'event')->count();

        $data = [
            'destination' => $destination,
            'culinary' => $culinary,
            'gallery' => $gallery,
            'event' => $event,
        ];

        return view('admin.index', $data);
    }
}
