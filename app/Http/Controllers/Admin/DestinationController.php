<?php

namespace App\Http\Controllers\Admin;

use App\Jobs\Articles\CreateArticle;
use App\Jobs\Articles\DeleteArticle;
use App\Jobs\Articles\UpdateArticle;
use App\Models\Contents\Article;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class DestinationController extends Controller
{
    protected $type = 'destination';

    public function __construct()
    {
        $this->middleware('auth.check:admin');
    }

    public function index()
    {
        $destination = Article::where('type', '=', $this->type)->paginate(10);

        $data = [
            'destination' => $destination,
        ];

        return view('admin.destination.index', $data);
    }

    public function create()
    {
        return view('admin.destination.create');
    }

    /**
     * @param Request $request
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'images.*' => 'required|image|mimes:jpeg,png,jpg,gif,svg',
            'cover.*' => 'required|image|mimes:jpeg,png,jpg,gif,svg',
            'title' => 'required',
            'description' => 'required',
            'location' => 'required',
        ]);

        if ($validator->fails() == true) {
            return redirect()->back()->withErrors($validator->errors());
        }

        $request->type = $this->type;

        try {
            $this->dispatch(new CreateArticle($request));
        } catch (\Exception $exception) {
            return redirect()->back()->withErrors($exception);
        }

        return redirect()->route('admin.destinasi')->with('success', 'Destinasi berhasil dibuat');
    }

    public function edit($slug)
    {
        $destination = Article::where('type', '=', $this->type)
            ->where('slug', '=', $slug)->first();

        if ($destination->count() == 0) {
            return redirect()->route('admin.destinasi')->withErrors('Destinasi tidak ada');
        }

        $data = [
            'destination' => $destination
        ];

        return view('admin.destination.edit', $data);
    }

    public function update($slug, Request $request)
    {
        $destination = Article::where('type', '=', $this->type)
            ->where('slug', '=', $slug)->first();

        if ($destination->count() == 0) {
            return redirect()->back()->withErrors('Destinasi tidak ada');
        }

        if ($destination instanceof Article) {

            $validator = Validator::make($request->all(), [
                'title' => 'required',
                'description' => 'required',
                'location' => 'required',
            ]);

            if ($validator->fails()) {
                return redirect()->back()->withErrors([$validator->errors()]);
            }

            try {
                $update = $this->dispatch(new UpdateArticle($request, $destination));
            } catch (\Exception $exception) {
                return redirect()->back()->withErrors(['error' => $exception]);
            }

            return redirect()->route('admin.destinasi')->with('success', 'Destinasi berhasil di edit');
        }

        return redirect()->back()->withErrors('Destinasi salah');
    }

    public function destroy($slug)
    {
        $destination = Article::where('type', '=', $this->type)
            ->where('slug', '=', $slug)->first();

        if ($destination->count() == 0) {
            return redirect()->route('admin.destinasi')->withErrors(['status' => 'Destinasi tidak ada']);
        }

        try {
            $delete = $this->dispatch(new DeleteArticle($destination));
        } catch (\Exception $exception) {
            return redirect()->back()->withErrors(['error' => $exception]);
        }

        return redirect()->route('admin.destinasi')->with('success', 'Destinasi berhasil di delete');
    }

    public function status($slug ,Request $request)
    {
        $destination = Article::where('type', '=', $this->type)
            ->where('slug', '=', $slug)->first();

        try {
            $destination->status = $request->status;
            $destination->save();
        } catch (\Exception $exception) {
            return back()->withErrors(['error' => $exception]);
        }

        return redirect()->route('admin.destinasi')->with('success', 'Status berhasil di perbarui');
    }
}