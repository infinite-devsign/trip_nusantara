<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::group([
    'namespace' => 'Guest'
], function () {
    Route::get('/', 'IndexController@index')->name('index');

    Route::group([
        'prefix' => 'destinasi',
        'as' => 'destination.'
    ], function () {
        $this->get('/', 'DestinationController@index')->name('index');
        $this->get('/{destination}', 'DestinationController@show')->name('show');
    });

    Route::group([
        'prefix' => 'kuliner',
        'as' => 'culinary.'
    ], function () {
        $this->get('/', 'CulinaryController@index')->name('index');
        $this->get('/{culinary}', 'CulinaryController@show')->name('show');
    });

    Route::group([
        'prefix' => 'galeri',
        'as' => 'gallery.'
    ], function () {
        $this->get('/', 'GalleryController@index');
        $this->get('/{gallery}', 'GalleryController@show')->name('show');
    });
});
Route::group([
   'prefix' => 'event'
], function () {
  $this->get('/', function() {
    return view('event.index');
  });
  $this->get('/festifal-bau', function() {
    return view('article.event1');
  });
  $this->get('/jakarta-fashion', function() {
    return view('article.event2');
  });
  $this->get('/art-jog', function() {
    return view('article.event3');
  });
});

//Route::post('gallery/store', 'Admin\GalleryController@store')->middleware('guest');

Route::get('/image{src}', 'MediaController@file')->where(['src' => '.*'])->name('image');

Route::group([
   'namespace' => 'Auth'
], function () {
    // Authentication Routes...
    $this->get('login', 'LoginController@showLoginForm')->name('login');
    $this->post('login', 'LoginController@login');
    $this->post('logout', 'LoginController@logout')->name('logout');

    $this->get('admin/login', 'AdminController@showLoginForm')->name('admin.login');
    $this->post('admin/login', 'AdminController@login')->name('admin.login');

    // Registration Routes...
    $this->get('register', 'RegisterController@showRegistrationForm')->name('register');
    $this->post('register', 'RegisterController@register');

    //verification Email
    $this->get('/verifyemail/{token}', 'RegisterController@verify');

    // Password Reset Routes...
    $this->get('password/reset', 'ForgotPasswordController@showLinkRequestForm')->name('password.request');
    $this->post('password/email', 'ForgotPasswordController@sendResetLinkEmail')->name('password.email');
    $this->get('password/reset/{token}', 'ResetPasswordController@showResetForm')->name('password.reset');
    $this->post('password/reset', 'ResetPasswordController@reset');
});


// Admin Route
Route::group([
    'prefix' => 'admin',
    'as' => 'admin.',
    'namespace' => 'Admin',
    'middleware' => 'auth.check:admin'
], function (){
    $this->get('/', 'DashboardController@index');
    $this->get('/pengaturan', 'SettingController@index')->name('setting');
    $this->post('/pengaturan/update', 'SettingController@update')->name('setting.update');
    $this->post('/pengaturan/avatar', 'SettingController@updateAvatar')->name('setting.avatar');
    $this->get('/logout', function () {
        \Illuminate\Support\Facades\Auth::logout();
        return redirect('/');
    })->name('logout');

    $this->group([
        'prefix' => '/destinasi',
    ], function () {
        $this->get('/', 'DestinationController@index')->name('destinasi');
        $this->get('/tambah', 'DestinationController@create')->name('destinasi.create');
        $this->post('/store', 'DestinationController@store')->name('destinasi.store');
        $this->get('/{slug}/edit', 'DestinationController@edit')->name('destinasi.edit');
        $this->post('/{slug}/update', 'DestinationController@update')->name('destinasi.update');
        $this->delete('/{slug}/delete', 'DestinationController@destroy')->name('destinasi.delete');
        $this->post('/{slug}/status', 'DestinationController@status')->name('destinasi.status');
    });

    $this->group([
        'prefix' => '/galeri',
    ], function () {
        $this->get('/', 'GalleryController@index')->name('galeri');
        $this->get('/tambah', 'GalleryController@create')->name('galeri.create');
        $this->post('/store', 'GalleryController@store')->name('galeri.store');
        $this->get('/{slug}/edit', 'GalleryController@edit')->name('galeri.edit');
        $this->post('/{slug}/update', 'GalleryController@update')->name('galeri.update');
        $this->delete('/{slug}/delete', 'GalleryController@destroy')->name('galeri.delete');
        $this->post('/{slug}/status', 'GalleryController@status')->name('galeri.status');
    });

    $this->group([
        'prefix' => '/event',
    ], function () {
        $this->get('/', 'EventController@index')->name('event');
        $this->get('/tambah', 'EventController@create')->name('event.create');
        $this->post('/store', 'EventController@store')->name('event.store');
        $this->get('/{slug}/edit', 'EventController@edit')->name('event.edit');
        $this->post('/{slug}/update', 'EventController@update')->name('event.update');
        $this->delete('/{slug}/delete', 'EventController@destroy')->name('event.delete');
        $this->post('/{slug}/status', 'EventController@status')->name('event.status');
    });

    $this->group([
        'prefix' => '/kuliner',
    ], function () {
        $this->get('/', 'CulinaryController@index')->name('kuliner');
        $this->get('/tambah', 'CulinaryController@create')->name('kuliner.create');
        $this->post('/store', 'CulinaryController@store')->name('kuliner.store');
        $this->get('/{slug}/edit', 'CulinaryController@edit')->name('kuliner.edit');
        $this->post('/{slug}/update', 'CulinaryController@update')->name('kuliner.update');
        $this->delete('/{slug}/delete', 'CulinaryController@destroy')->name('kuliner.delete');
        $this->post('/{slug}/status', 'CulinaryController@status')->name('kuliner.status');
    });
});

Route::group([
    'namespace' => 'Member',
    'middleware' => 'auth.check:member',
    'prefix' => '/home',
    'as' => 'member.',
], function () {
    $this->get('/', 'DashboardController@index');
    $this->get('/pengaturan', 'SettingController@index')->name('setting');
    $this->post('/pengaturan/update', 'SettingController@update')->name('setting.update');
    $this->post('/pengaturan/avatar', 'SettingController@updateAvatar')->name('setting.avatar');
    $this->get('/logout', function () {
        \Illuminate\Support\Facades\Auth::logout();
        return redirect('/');
    })->name('logout');

    $this->group([
        'prefix' => '/destinasi',
    ], function () {
        $this->get('/', 'DestinationController@index')->name('destinasi');
        $this->get('/tambah', 'DestinationController@create')->name('destinasi.create');
        $this->post('/store', 'DestinationController@store')->name('destinasi.store');
        $this->get('/{slug}/edit', 'DestinationController@edit')->name('destinasi.edit');
        $this->post('/{slug}/update', 'DestinationController@update')->name('destinasi.update');
        $this->delete('/{slug}/delete', 'DestinationController@destroy')->name('destinasi.delete');
    });

    $this->group([
        'prefix' => '/galeri',
    ], function () {
        $this->get('/', 'GalleryController@index')->name('galeri');
        $this->get('/tambah', 'GalleryController@create')->name('galeri.create');
        $this->post('/store', 'GalleryController@store')->name('galeri.store');
        $this->get('/{slug}/edit', 'GalleryController@edit')->name('galeri.edit');
        $this->post('/{slug}/update', 'GalleryController@update')->name('galeri.update');
        $this->delete('/{slug}/delete', 'GalleryController@destroy')->name('galeri.delete');
    });

    $this->group([
        'prefix' => '/event',
    ], function () {
        $this->get('/', 'EventController@index')->name('event');
        $this->get('/tambah', 'EventController@create')->name('event.create');
        $this->post('/store', 'EventController@store')->name('event.store');
        $this->get('/{slug}/edit', 'EventController@edit')->name('event.edit');
        $this->post('/{slug}/update', 'EventController@update')->name('event.update');
        $this->delete('/{slug}/delete', 'EventController@destroy')->name('event.delete');
    });

    $this->group([
        'prefix' => '/kuliner',
    ], function () {
        $this->get('/', 'CulinaryController@index')->name('kuliner');
        $this->get('/tambah', 'CulinaryController@create')->name('kuliner.create');
        $this->post('/store', 'CulinaryController@store')->name('kuliner.store');
        $this->get('/{slug}/edit', 'CulinaryController@edit')->name('kuliner.edit');
        $this->post('/{slug}/update', 'CulinaryController@update')->name('kuliner.update');
        $this->delete('/{slug}/delete', 'CulinaryController@destroy')->name('kuliner.delete');
    });
});
