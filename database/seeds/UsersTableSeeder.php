<?php

use App\Models\Accounts\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    protected $data = [
        [
            'name' => 'Admin Ganteng',
            'username' => 'admin_ganteng',
            'email' => 'gantengadmin@tripnusantara.ga',
            'password' => 'secretadmin',
            'role' => 'admin',
            'status' => 'active'
        ],
        [
            'name' => 'Member biasa',
            'username' => 'sayamember',
            'email' => 'member@email.com',
            'password' => 'member123',
        ]
    ];
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->data as $datum) {
            User::create($datum);
        }
    }
}
