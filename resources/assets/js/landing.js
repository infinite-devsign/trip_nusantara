var scrollPosition = $(window).scrollTop();
scrollAnim();
var scrollPosition = null;
var landingScroll;
$(window).scroll(function (event) {
    landingScroll = $(window).scrollTop();
    console.log(landingScroll);
    scrollAnim(landingScroll);
});
function scrollAnim(scroll) {
    if (landingScroll > 467 || scrollPosition > 467) {
        $('.layer1 .content .box').css({'margin-bottom':'12px', 'margin-top':'12px', 'opacity':'1', 'transition': '0.6s'});
        $('.layer1 .content .box:nth-child(2)').css({'transition-delay':'0.1s'});
        $('.layer1 .content .box:nth-child(3)').css({'transition-delay':'0.2s'});
        $('.layer1 .content .box:nth-child(4)').css({'transition-delay':'0.3s'});
    } if (landingScroll > 1170 || scrollPosition > 1170) {
        $('.layer2 .box').css({'transform':'scale(1)', 'transition': '0.6s'});
    } if (landingScroll > 1800 || scrollPosition > 1800) {
        $('.layer3 .content .box').css({'transform':'scale(1)', 'transition':'0.5s ease'});
        $('.layer3 .content .box:nth-child(2)').css({'transition-delay':'0.2s'});
        $('.layer3 .content .box:nth-child(3)').css({'transition-delay':'0.4s'});
    } if (landingScroll > 2271 || scrollPosition > 2271) {
        $('.layer4 .content .bottom .box').css({'opacity':'1', 'transition':'0.4s ease'})
        $('.layer4 .content .bottom .box:nth-child(2)').css({'transition-delay':'0.1s'})
        $('.layer4 .content .bottom .box:nth-child(3)').css({'transition-delay':'0.2s'})
        $('.layer4 .content .bottom .box:nth-child(4)').css({'transition-delay':'0.3s'})
        $('.layer4 .content .bottom .box:nth-child(5)').css({'transition-delay':'0.4s'})
    }
}
