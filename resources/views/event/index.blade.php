@extends('layouts.app')
@section('header')
  <link rel="stylesheet" href="{{ asset('css/listPage.css') }}">
  <style media="screen">
    body {
      background: #eee url("{{ asset('img/patter.png') }}") !important;
    }
    </style>
@endsection
@section('navbar-event')
  active
@endsection
@section('nav-color')
  "linear-gradient(to right, #f09819, #edde5d)"
@endsection
@section('content')
  @include('elements.navbar')
  <div class="cover">
    <div class="filter"></div>
    <img src="{{ asset('img/article/event/festifal-bau2.jpg') }}" alt="">
  </div>
  <div class="header">
    <span>
      <h1>Indonesia Culture Event</h1>
    </span>
    {{-- <a onclick="dropdown()" class="dropdown btn red">Test</a>
    <div id="myDropdown" class="dropdown-content">
      <a href="#home">Pengaturan</a>
      <a href="#about">Keluar</a>
    </div> --}}
  </div>
  <div class="content">
    <div class="box">
      <div class="img">
        <img class="lazyload" src="{{ asset('img/article/event/festifal-bau.jpg') }}" data-original="{{ asset('img/article/event/festifal-bau.jpg') }}" alt="">
      </div>
      <div class="article">
        <div class="description">
          <span class="span1">Unknown</span>
          <span class="span2">Post On: 26 Maret 2018</span>
          <span class="span3">Like 0 Comment 0</span>
        </div>
        <div class="title">
          <h3><a href="{{ url('event/festifal-bau') }}">Festival Bau Nyale yang Menyenangkan 2018 di Pulau Lombok yang Memikat</a></h3>
        </div>
      </div>
    </div>
    <div class="box">
      <div class="img">
        <img class="lazyload" src="{{ asset('img/article/event/jakarta-fashion.jpg') }}" data-original="{{ asset('img/article/event/jakarta-fashion.jpg') }}" alt="">
      </div>
      <div class="article">
        <div class="description">
          <span class="span1">05 April 2018</span>
          <span class="span2">Post On: 26 Maret 2018</span>
          <span class="span3">Like 0 Comment 0</span>
        </div>
        <div class="title">
          <h3><a href="{{ url('event/jakarta-fashion') }}">Nikmati Jakarta Fashion & Food Festival 2018</a></h3>
        </div>
      </div>
    </div>
    <div class="box">
      <div class="img">
        <img class="lazyload" src="{{ asset('img/article/event/artjog-1.jpg') }}" data-original="{{ asset('img/article/event/artjog-1.jpg') }}" alt="">
      </div>
      <div class="article">
        <div class="description">
          <span class="span1">04 Mei 2018</span>
          <span class="span2">Post On: 26 Maret 2018</span>
          <span class="span3">Like 0 Comment 0</span>
        </div>
        <div class="title">
          <h3><a href="{{ url('event/art-jog') }}">Art Jog 2018: Pameran Seni yang Mengagumkan di Jantung Jawa</a></h3>
        </div>
      </div>
    </div>
  </div>
@endsection
