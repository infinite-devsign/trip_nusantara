@extends('layouts.app')
@section('header')
  <link rel="stylesheet" href="{{ asset('css/details.css') }}">
  <style media="screen">
    body {
      background: #eee url("{{ asset('img/patter.png') }}") !important;
    }
    .container-content img {
      max-width: 100%;
    }
    .review {
      margin-bottom: 20px;
    }
    .sidenav, .side-min {
      background: linear-gradient(to right, #f09819, #edde5d) !important;
    }
  </style>
@endsection
@section('nav-color')
  "linear-gradient(to right, #f09819, #edde5d)"
@endsection
@section('content')
@include('elements.navbar')

<div class="container">
  <div class="article-container">
    <div class="article">
      <div class="header">
        <div class="date-container">
          <div class="date">
            @yield('event-date')
          </div>
        </div>
        <div class="title">
          <h1>@yield('event-title')</h1>
        </div>
      </div>
      <div class="content">
        <div class="cover">
          <p><img src="@yield('event-image')" alt=""></p>
        </div>
        <div class="container-content">
          @yield('event-content')
        </div>
      </div>
    </div>
  </div>
  <div class="widget-container">
    <div class="widget">
      <h3>Di Bulan Ini</h3>
      <div class="list-destination">
        <div class="img-container">
          <img src="{{ asset('img/article/event/festifal-bau.jpg') }}" alt="">
        </div>
        <div class="name-container">
          <a href="#">Festival Bau Nyale</a>
        </div>
      </div>
      <div class="list-destination">
        <div class="img-container">
          <img src="{{ asset('img/article/event/jakarta-fashion.jpg') }}" alt="">
        </div>
        <div class="name-container">
          <a href="#">Jakarta Fashion & Food </a>
        </div>
      </div>
      <div class="list-destination">
        <div class="img-container">
          <img src="https://upload.wikimedia.org/wikipedia/commons/d/d8/Mtbromo.jpg" alt="">
        </div>
        <div class="name-container">
          <a href="#">Gunung Bromo</a>
        </div>
      </div>
    </div>
  </div>

  <div class="review">
    <h3>Komentar</h3>
    <div class="list-review-container">
      <p style="margin:20px">Belum ada Komentar</p>
      {{-- <div class="list-review">
        <div class="user-img">
          <img src="{{ asset('img/Fariz.jpg') }}" alt="">
        </div>
        <div class="content-review">
          <h4>Nizar Alfarizi Akbar</h4>
          <div class="this-review">
            ga pernah naik gunung mas :D.
          </div>
        </div>
      </div>
      <div class="list-review">
        <div class="user-img">
          <img src="{{ asset('img/Fariz.jpg') }}" alt="">
        </div>
        <div class="content-review">
          <h4>Nizar Alfarizi Akbar</h4>
          <div class="this-review">
            ga pernah naik gunung mas :D.
          </div>
        </div>
      </div>
      <div class="list-review">
        <div class="user-img">
          <img src="{{ asset('img/Fariz.jpg') }}" alt="">
        </div>
        <div class="content-review">
          <h4>Nizar Alfarizi Akbar</h4>
          <div class="this-review">
            ga pernah naik gunung mas :D.
          </div>
        </div>
      </div> --}}
    </div>
  </div>

</div>
@endsection
