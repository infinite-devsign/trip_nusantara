@extends('layouts/app')
@section('navbar-beranda')
active
@endsection
@section('header')
    <link rel="stylesheet" href="{{ asset('css/landing.css') }}">
@endsection
@section('nav-color')
  "white"
@endsection
@section('content')
    @include('elements/navbar')
    <div class="cover">
        <div class="text">
            <h1>Liwung</h1>
            <h2>Indonesia itu luas, kamu ga kuat, biar aku ajah.</h2>
        </div>
        <div class="house"></div>
        <div class="filter"></div>
    </div>
    <div class="layer1">
        <div class="bg"></div>
        <h3>Destinasi</h3>
        <h5>Perjalanan hebat butuh tempat untuk berhenti.</h5>
        <div class="content">
            @foreach($destination as $key => $value)
            <div class="box">
              <a href="{{ route('destination.show', $value->slug) }}">
                <img class="lazyload" src="{{ route('image', $value->media->first()->src) }}" data-original="{{ route('image', $value->media->first()->src) }}" alt="">
                <a href="{{ route('destination.show', $value->slug) }}" style="text-decoration: none; color: black !important;">
                  <p>{{$value->title}}</p>
                </a>
              </a>
            </div>
            @endforeach
        </div>
    </div>
    <div class="layer2">
        <div class="bg"></div>
        <img class="harta" src="img/harta-karun.png" alt="">
        <div class="box">
            <h5>Pilih salah satu kota</h5>
            <div class="box-content">
                <div class="city">
                    <a href="#"><img src="{{ asset('img/icon/jakarta_ico.png') }}" alt=""></a>
                    <a href="#">Jakarta</a>
                </div>
                <div class="city">
                    <a href="#"><img src="{{ asset('img/icon/bandung_ico.png') }}" alt=""></a>
                    <a href="#">Bandung</a>
                </div>
                <div class="city">
                    <a href="#"><img src="{{ asset('img/icon/surabaya_ico.png') }}" alt=""></a>
                    <a href="#">Surabaya</a>
                </div>
                <div class="city">
                    <a href="#"><img src="{{ asset('img/icon/jogja_ico.png') }}" alt=""></a>
                    <a href="#">Yogyakarta</a>
                </div>
                <div class="city">
                    <a href="#"><img src="{{ asset('img/icon/bali_ico.png') }}" alt=""></a>
                    <a href="#">Bali</a>
                </div>
                <div class="city">
                    <a href="#"><img src="{{ asset('img/icon/makassar_ico.png') }}" alt=""></a>
                    <a href="#">Makassar</a>
                </div>
                <a class="btn btn-round blue city-more" href="#">Lainnya</a>
            </div>
        </div>
    </div>
    <div class="layer3">
        <div class="bg"></div>
        <div class="top">
            <div class="col-100">
                <h5>Nusantara Event</h5>
            </div>
        </div>
        <div class="content">
            <a class="box" href="{{ url('event/festifal-bau') }}">
                <div class="">
                      <span class="date">
                        <div class="">02</div>
                        <div class="">April</div>
                        <div class="">2018</div>
                      </span>
                    <img class="lazyload" src="{{ asset('img/article/event/festifal-bau.jpg') }}" data-original="{{ asset('img/article/event/festifal-bau.jpg') }}"
                         alt="">
                    <p>Festival Bau Nyale yang Menyenangkan 2018 di Pulau Lombok yang Memikat</p>
                </div>
            </a>
            <a class="box" href="{{ url('event/jakarta-fashion') }}">
                <div class="">
                    <span class="date">
                      <div class="">05</div>
                      <div class="">April</div>
                      <div class="">2018</div>
                    </span>
                    <img class="lazyload" src="{{ asset('img/article/event/jakarta-fashion.jpg') }}" data-original="{{ asset('img/article/event/jakarta-fashion.jpg') }}"
                         alt="">
                    <p>Nikmati Jakarta Fashion & Food Festival 2018 -</p>
                </div>
            </a>
            <a class="box" href="{{ url('event/art-jog') }}">
                <div class="">
                    <span class="date">
                      <div class="">04</div>
                      <div class="">Mei</div>
                      <div class="">2018</div>
                    </span>
                    <img class="lazyload" src="{{ asset('img/article/event/artjog-1.jpg') }}" data-original="{{ asset('img/article/event/artjog-1.jpg') }}"
                         alt="">
                    <p>Art Jog 2018: Pameran Seni yang Mengagumkan di Jantung Jawa</p>
                </div>
            </a>
        </div>
        <div class="bottom">
            <a href="{{ url('event') }}" class="btn btn-round red">Lihat Semuanya</a>
        </div>
    </div>
    <div class="layer4">
        <div class="content">
            <div class="top">
                <div class="tempelan">
                    <p>Tempelan</p>
                </div>
            </div>
            <div class="bottom">
            @foreach($gallery as $value)
              <div class="box">
                <div class="content-container">
                  <div class="img">
                    <img class="lazyload" src="{{ route('image', $value->media->first()->src) }}" data-original="{{ route('image', $value->media->first()->src) }}" alt="">
                  </div>
                </div>
                <div class="content-container">
                  <div class="description">
                    <a href="{{ route('gallery.show', $value->slug) }}">{{ $value->title }}</a>
                  </div>
                </div>
              </div>
            @endforeach
            </div>
            <div class="bottom-2">
                <a href="{{ url('galeri') }}" class="btn green">Lebih Banyak</a>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
<script type="text/javascript" src="{{ asset('js/landing.js') }}"></script>
@endsection
