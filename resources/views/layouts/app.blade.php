<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
@include('elements/header')
@yield('header')
</head>
<body>
@yield('content')


@include('elements/footer')
    <!-- Scripts -->
@include('elements/scripts')
@yield('scripts')
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAHqhoNLuhSNDc9cylimR7xCCvA8Z77Rw0&callback=initMap"
        async defer></script>
</body>
</html>
