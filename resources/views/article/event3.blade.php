<?php
/**
 * Created by PhpStorm.
 * User: ANDROMEDA
 * Date: 3/27/2018
 * Time: 8:03 PM
 */
?>
@extends('event.details')
@section('navbar-event')
    active
@endsection
@section('title')
    Art Jog 2018: Pameran Seni yang Mengagumkan di Jantung Jawa -
@endsection
@section('event-date')
    4<br>Mei<br>2018
@endsection
@section('event-title')
    Art Jog 2018: Pameran Seni yang Mengagumkan di Jantung Jawa
@endsection
@section('event-image')
    {{ asset('img/article/event/artjog-1.jpg') }}
@endsection
@section('event-content')
    <p>Sumber Gambar : <a href="www.artjog.co.id">www.artjog.co.id</a></p>
    <p>Sebagai jantung budaya Jawa, Yogyakarta (atau Jogjakarta) selalu terkenal dengan adegan
        seninya. Beberapa nama yang paling menonjol di TKP berasal dari sini. Bekas ibukota
        Indonesia terus menjadi salah satu pusat seni penting di Indonesia. Bagi mereka yang
        menghargai keindahan seni dalam berbagai bentuknya, <b>Pameran ART JOG</b> ke-11 akan
        berlangsung dari 4 Mei hingga 4 Juni 2018 di Museum Nasional Jogja.</p>
    <p>Acara tahunan terus menawarkan perspektif yang berbeda untuk melihat dunia melalui
        seni. Acara ini akan diikuti oleh seniman, kolektor seni, dan eksekutif galeri dari
        Indonesia dan seluruh dunia. Beberapa nama yang menyoroti pameran tahun ini adalah:
        Adam de Boer (AS), Ezzam Rahman (Singapura), Hiromi Tango (Australia),
        Kexin Zhang (Cina), Marcel Schwittlick (Jerman), Ronald Ventura (Filipina), Bakudapan
        Food Story Group & Fajar Riyanto (Yogyakarta / Indonesia), Fajar Abadi (Bandung /
        Indonesia), Agung Tato Suryanto (Surabaya / Indonesia), dan banyak lagi.</p>
    <p><img src="{{ asset('img/article/event/artjog-2.jpg') }}" alt=""></p>
    <p>Sumber Gambar : <a href="www.artjog.co.id">www.artjog.co.id</a></p>
    <p>Melalui aplikasi panggilan terbuka, ART JOG memberi peluang bagi seniman baru dan
        publik, secara umum, untuk berpartisipasi dengan mengirimkan karya seni mereka yang
        akan dipilih dan dikuratori oleh para kurator. Pada saat yang sama, ini juga merupakan
        cara untuk mencari bakat yang akan datang dari semua bentuk seni karena
        mengakomodasi berbagai jenis media, mulai dari lukisan, gambar, foto, instalasi,
        multi-dimensi, patung, mekanik-kinetik, seni pertunjukan, dan banyak lagi.</p>
    <p><img src="{{ asset('img/article/event/artjog-3.jpg') }}" alt=""></p>
    <p>Sumber Gambar : <a href="www.artjog.co.id">www.artjog.co.id</a></p>
    <p>Selama bertahun-tahun, salah satu atraksi paling terkenal ART JOG adalah transformasi
        halaman depan tempat pertunjukan. ARTJOG mengundang seorang individu atau sekelompok
        seniman untuk membuat muka bangunan depan dan / atau halaman depan, tempat pameran
        diadakan, sesuai dengan tema utama "Pencerahan". Ini adalah ikon ART JOG dan tidak
        pernah gagal menarik perhatian para pengunjungnya.</p>
    <p><img src="{{ asset('img/article/event/artjog-4.jpg') }}" alt=""></p>
    <p>Sumber Gambar : <a href="www.artjog.co.id">www.artjog.co.id</a></p>
    <p>Sebagai sebuah acara seni, ART JOG telah mendedikasikan dirinya selama 10 tahun untuk
        membangun jaringan lokal dan menciptakan koneksi komunitas seni visual di seluruh dunia.
        Bagi pengunjung, ini adalah kesempatan sempurna untuk mengamati karya seni terbaik dari
        seluruh dunia. Kesempatan untuk melihat bagaimana seni meniru kehidupan dan bagaimana estetik
        didefinisikan dengan berbagai cara.</p>
    <p><img src="{{ asset('img/article/event/artjog-5.jpg') }}" alt=""></p>
    <p>Sumber Gambar : <a href="www.artjog.co.id">www.artjog.co.id</a></p>
@endsection
