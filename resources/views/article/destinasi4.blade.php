@extends('destination.details')
@section('title')
Raja Ampat -
@endsection
@section('navbar-destinasi')
active
@endsection
@section('title-destination')
Raja Ampat
@endsection
@section('image-destination')
{{ asset('img/article/destination/raja-ampat.jpg') }}
@endsection
@section('destination')
  <p>Kepulauan Raja Ampat di Papua Barat menyimpan potensi wisata yang luar biasa.
Konon, tempat wisata ini merupakan lokasi penyelaman bawah laut terbaik di dunia
karena keberagaman fauna dan flora bawah air yang dimilikinya. Menurut peneliti,
75% dari total terumbu karang di seluruh dunia terdapat di Raja Ampat. Tidak
heran kalau banyak pelancong asing yang tergila-gila dengan keindahan wisata
Raja Ampat. Jika berkunjung ke sini, cobalah menginap di kapal pinisi yang
terdapat di lokasi. Meskipun harganya cukup mahal (Rp100 juta untuk 14 orang
selama seminggu), Anda akan mendapatkan pengalaman berwisata yang tak
terlupakan. Akses menuju Raja Ampat cukup mudah, dari Bandara Domine Eduard
Osok di Sorong, perjalanan dilanjutkan dengan menggunakan angkutan umum
menuju Pelabuhan Sorong. Jangan lupa, di sini Anda harus mendaftarkan diri
untuk bisa masuk kawasan wisata Raja Ampat. Dari Pelabuhan Sorong, Anda bisa
menaiki kapal rakyat menuju ibu kota Raja Ampat dan mencari penginapan murah
di sana. Nah, untuk menuju lokasi penyelaman, Anda bisa
menyewa speedboatyang disediakan di sekitar lokasi.</p>
<p>
  Sumber Artikel : <a href="https://www.traveloka.com/activities/indonesia">https://www.traveloka.com/activities/indonesia</a>
</p>
@endsection
