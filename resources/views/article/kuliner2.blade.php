@extends('culinary.details')
@section('navbar-kuliner')
  active
@endsection
@section('title')
Jajan Pasar: Kudapan Tradisional Cita Rasa Aduhai -
@endsection
@section('culinary-title')
Jajan Pasar: Kudapan Tradisional Cita Rasa Aduhai
@endsection
@section('culinary-image')
{{ asset('img/article/kuliner/jajan-pasar.jpg') }}
@endsection
@section('culinary-content')
<p>Sumber Gambar : <a href="https://assets.kompasiana.com/statics/files/14206473481245144984.jpg?t=o&v=700?t=o
&v=700">https://assets.kompasiana.com/statics/files/14206473481245144984.jpg?t=o&v=700?t=o
&v=700</a> </p>
<p>Jeli mencermati fenomena kemunculan jajanan pasar di pasar
modern bahkan mal, Dewi Pagi (penulis) menulis ulasan kuliner
mengangkat aneka kue tradisional. Baginya, rasa yang ‘original’
membuat jajanan pasar tak pernah ditinggalkan orang-orang.
Indonesia yang kaya akan warisan kulinernya bahkan memiliki
ribuan lebih jenis kue-kue tradisionalnya. Setelah memamaparkan
jelajah rasanya, penulis juga mengajak pembaca untuk berbagi cerita
kue tradisional lewat tulisan ini. Tujuannya, sama-sama belajar
bahkan melestarikan kuliner nusantara terutama kue tradisional.
Topik ini pun berhasil mendatangkan banyak pembaca di 2014.</p>
@endsection
