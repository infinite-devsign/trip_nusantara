@extends('destination.details')
@section('title')
Nusa Dua -
@endsection
@section('navbar-destinasi')
active
@endsection
@section('title-destination')
Nusa Dua
@endsection
@section('image-destination')
{{ asset('img/article/destination/nusa-dua.jpg') }}
@endsection
@section('destination')
  <p>Usai puas menjelajah Kalimantan, mari bergeser ke Pulau Dewata, Bali. Siapa
yang tak terkenang dengan keindahan Pulau Seribu Dewa yang satu ini?
Pesonanya yang begitu memikat pun sampai membuat wisatawan mancanegara
berbondong-bondong melancong ke sini setiap tahunnya. Nah, di Bali ada satu
tempat wisata yang begitu cantik, yakni Nusa Dua. Objek wisata pantai ini memiliki
pasir putih yang lembut dengan air lautnya berwarna biru jernih. Selain bisa
berselancar di pantainya, Anda juga akan dimanjakan dengan berbagai fasilitas
yang ada di sekitar lokasi, mulai dari penginapan dan resort berkelas internasional,
restoran, pusat-pusat perbelanjaan, dan masih banyak lagi yang lainnya.</p>
<p>
  Sumber Artikel : <a href="https://www.traveloka.com/activities/indonesia">https://www.traveloka.com/activities/indonesia</a>
</p>
@endsection
