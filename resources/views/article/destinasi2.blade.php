@extends('destination.details')
@section('title')
Taman Nasional Bromo Tengger Semeru -
@endsection
@section('navbar-destinasi')
active
@endsection
@section('title-destination')
Taman Nasional Bromo Tengger Semeru
@endsection
@section('image-destination')
{{ asset('img/article/destination/bromo.jpg') }}
@endsection
@section('destination')
  <p>Jawa Timur memiliki satu pesona wisata yang kecantikannya tak terbantahkan,
bahkan oleh wisatawan dari berbagai penjuru dunia. Ya, apa lagi kalau bukan
Taman Nasional Bromo Tengger Semeru? Tempat wisata berupa taman nasional
yang terletak di 4 kabupaten di Jawa Timur ini sendiri memiliki banyak objek
menarik yang tidak boleh dilewatkan, mulai dari Gunung Bromo dan semeru, lautan
pasir, Pantai Berbisik, Air Terjun Madakaripura, Ranu Pane, Ranu Kumbolo, dan
lain-lain yang semuanya cantik. Akses menuju TNBTS ini terbilang cukup mudah,
Anda hanya perlu mengikuti rute menuju Gunung Bromo. Meskipun jalannya cukup
ekstrem, semua akan terbayar tuntas jika Anda sudah sampai di lokasi.</p>
<p>
  Sumber Artikel : <a href="https://www.traveloka.com/activities/indonesia">https://www.traveloka.com/activities/indonesia</a>
</p>
@endsection
