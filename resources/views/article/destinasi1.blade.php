@extends('destination.details')
@section('title')
Danau Toba -
@endsection
@section('navbar-destinasi')
active
@endsection
@section('title-destination')
Danau Toba
@endsection
@section('image-destination')
{{ asset('img/article/destination/danau-toba.jpg') }}
@endsection
@section('destination')
  <p>Anda tentu sudah tak asing lagi dengan tempat wisata yang satu ini, bukan? Ya,
keindahan Danau Toba dengan Pulau Samosir di tengahnya memang berhasil
menarik perhatian wisatawan, tidak hanya lokal, tapi juga internasional. Danau
Toba sendiri merupakan sebuah danau raksasa yang terbentuk akibat dari aktivitas
gunung berapi. Danau ini memiliki panjang 100 km dan lebar 30 km dan disebutsebut
sebagai danau vulkanik terindah yang dimiliki Indonesia. Tidak hanya wisata
danaunya yang menarik, di sini pengunjung juga bisa menjelajah kawasan hutan
pinus di sekitar danau, menikmati pemandian air hangat, dan bermain-main di
sekitar air terjun. Sudah? Tunggu dulu, di Pulau Samosir ada 2 desa wisata yang
juga tak luput dari perhatian wisatawan, yakni Tomok dan Tuktuk. Di Tomok, Anda
bisa mengunjungi makam Raja Sidabutar, Museum Batak, dan melihat pertunjukan
boneka Sigale-gale. Mau bermalam di sini? Ada banyak penginapan murah di
Tuktuk yang bisa Anda jadikan referensi. Lengkap sekali, bukan?</p>
<p>
  Sumber Artikel : <a href="https://www.traveloka.com/activities/indonesia">https://www.traveloka.com/activities/indonesia</a>
</p>
@endsection
