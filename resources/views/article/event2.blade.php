@extends('event.details')
@section('navbar-event')
  active
@endsection
@section('title')
  Nikmati Jakarta Fashion & Food Festival 2018
@endsection
@section('event-date')
  05<br>April<br>2018
@endsection
@section('event-title')
Nikmati Jakarta Fashion & Food Festival 2018 -
@endsection
@section('event-image')
{{ asset('img/article/event/jakarta-fashion.jpg') }}
@endsection
@section('event-content')
<p>Sumber Gambar: <a href="4.bp.blogspot.com">4.bp.blogspot.com</a></p>
<p>Warna-warna cerah, desain berani, fashion yang luar biasa, couture yang berkelas,
masakan yang nikmat, dan menu yang istimewa sekali lagi akan menerangi dunia mode
dan kuliner Jakarta karena ibu kota ini akan menyelenggarakan Jakarta Fashion &
Food Festivalke-15 (JFFF) 2018 dari tanggal 5 April hingga 6 Mei di Summarecon,
Kelapa Gading Center, Jakarta Utara .</p>
<p>Festival besar ini akan menampilkan budaya Indonesia yang kaya dalam dua hal
utama: Festival Busana dan Festival Makanan. Namun, tidak seperti dua tahun lalu,
Gading Nite Carnival yang spektakuler, yang selalu menjadi bagian penting dari festival,
tidak akan berlangsung di sepanjang Boulevard Street tahun ini karena pembangunan
sistem transportasi Light Rail Transit (LRT) di seluruh kota . Sebagai gantinya,
pembukaan JFFF 2018 akan disorot dengan pertunjukan kembang api spektakuler
pada Sabtu, 7 April 2018 pukul 08.00 WIB.</p>
<h3>Etalase Budaya Beragam di Indonesia dalam Fashion</h3>
<p>The Fashion Festival akan menampilkan warisan Indonesia yang kaya dalam desain
mode modern, yang disajikan dalam koleksi siap pakai oleh desainer independen dan
desainer yang akan datang dari berbagai lembaga mode, serta haute couture oleh para
desainer top Indonesia. Festival Fashion akan berlangsung di catwalk Ballroom
di Harris Hotel and Convention, serta di The Forum di Kelapa Gading Mall , dan di
Fashion Village Trade Show of JFFF.</p>
<p>Di antara beberapa desainer top Indonesia yang akan memamerkan koleksi mereka
adalah: Albert Yanuar, Andreas Odang, Ariy Arka, Danny Satriadi, Eridani, Handy
Hartono, Kursen Karzai, Mel Ahyar, Stella Risa, dan banyak lagi.</p>
<p> <img src="{{ asset('img/article/event/jakarta-fashion2.jpg') }}" alt=""> </p>
<p> Sumber Gambar : <a href="life.108jakarta.com">life.108jakarta.com</a> </p>
<p>Mereka yang tertarik dengan industri fesyen, baik itu dalam pemodelan, kewirausahaan,
perawatan kecantikan atau kosmetik diundang untuk berpartisipasi dalam Pencarian
Model Gading, Anak-Anak Pencarian Model Gading, dan Kompetisi Desainer Muda
Yang Menjanjikan Berikutnya. Selanjutnya, sebagai penghargaan kepada individu dan
institusi yang telah banyak berkontribusi pada pertumbuhan industri fashion Indonesia,
Fashion Icon Awards akan dipentaskan sebagai akhir dari seri Fashion Extravaganza.</p>
<h3>Bermacam-macam Hidangan Lezat dengan Suasana Unik</h3>
<p>Menyoroti kekayaan dan keragaman masakan asli Indonesia, festival makanan akan
membawa Anda pada perjalanan yang menggugah selera di seluruh nusantara
melalui Kampoeng Tempoe Doloe (Desa Waktu Tua), yang akan berlangsung
berdampingan dengan cita rasa internasional dari Anggur & Keju Expo.</p>
<p> <img src="{{ asset('img/article/event/jakarta-fashion3.jpg') }}" alt=""> </p>
<p>Sumber Gambar:  <a href="arfathah.wordpress.com">arfathah.wordpress.com</a> </p>
<p>Tahun ini, Kampung Tempoe Doeloe (KTD) akan dipresentasikan dalam dekorasi
bertema taman tropis dengan aksen berbagai tanaman tropis dan bunga-bunga
indah. Tidak kurang dari 100 usaha kecil dan menengah, serta berbagai merek kuliner
ternama, akan menghadirkan lebih dari 200 menu tradisional asli Indonesia di festival
tersebut. Pecinta kuliner untuk mencari cita rasa asli Indonesia adalah untuk suguhan
nyata.</p>
<p> <img src="{{ asset('img/article/event/jakarta-fashion4.jpg') }}" alt=""> </p>
<p>Sumber Gambar: <a href="www.kompasiana.com">www.kompasiana.com</a> </p>
<p>Setelah keberhasilan kompetisi 'Noodle of the Archipelago' tahun lalu, JFFF akan
kembali menyoroti salah satu hidangan paling populer di Indonesia; dan kali ini, sorotan
beralih ke Soto atau sup tradisional yang terbuat dari kaldu, daging, dan
sayuran. Kompetisi Soto sejalan dengan program Dewan Ekonomi Kreatif Indonesia
untuk mempromosikan Soto ke seluruh dunia dengan slogan yang menarik: 'A Spoonful
of Indonesian Warmth'. Babak pra-kualifikasi untuk kompetisi itu sendiri telah dimulai
sejak akhir tahun 2017. Tim ahli telah mencari Soto terbaik di sejumlah kota
termasuk Solo , Medan , Makassar , Kudus, Semarang , dan Surabaya.. Sebanyak 10
koki Soto dipilih untuk berkompetisi di JFFF tahun ini.
Sebagai bagian yang tidak terpisahkan dari Festival Makanan, Pameran Anggur & Keju
juga akan berlangsung tahun ini. Pameran ini akan menghadirkan berbagai macam
anggur berkualitas tinggi dari berbagai negara termasuk Amerika, Argentina, Australia,
Kanada, Austria, Chili, Prancis, Jerman, Italia, Selandia Baru, Portugal, dan Afrika
Selatan. Juga akan ada produsen dan distributor produk anggur dan keju terkenal, serta
pakar kuliner internasional. Anda dapat menikmati mencicipi anggur dan keju, dan
mendapatkan penawaran bagus untuk pembelian produk tertentu.
Sebagai acara luar biasa yang menjelajahi dunia mode dan kuliner Indonesia yang
beragam, Jakarta Fashion and Food Festival adalah acara yang tidak ingin Anda
lewatkan. Sampai jumpa!</p>
<p>Sumber Artikel : <a href=" http://www.indonesia.travel/gb/en/event-festivals/experience-thejakarta-fashion-and-food-festival-2018"> http://www.indonesia.travel/gb/en/event-festivals/experience-thejakarta-fashion-and-food-festival-2018</a> </p>
@endsection
