@extends('destination.details')
@section('title')
Kepulauan Karimun Jawa -
@endsection
@section('navbar-destinasi')
active
@endsection
@section('title-destination')
Kepulauan Karimun Jawa
@endsection
@section('image-destination')
{{ asset('img/article/destination/karimun-jawa.jpg') }}
@endsection
@section('destination')
  <p>Di Provinsi Jawa Tengah, tepatnya di bagian utara Pulau Jawa, ada satu tempat
wisata menarik yang banyak dirujuk wisatawan sebagai destinasi liburan akhir
pekan, tidak hanya wisatawan domestik, tapi juga mancanegara, yakni Kepulauan
Karimun Jawa. Tempat wisata yang masuk dalam wilayah Kabupaten Jepara ini
merupakan titik paling pas untuk menikmati surga bawah laut dengan 90 jenis
karang laut dan 240 lebih spesies ikan yang luar biasa cantik. Selain itu, Anda juga
bisa menikmati hutan mangrove dan melihat satwa khas di tempat wisata yang
dijuluki sebagai Pulau Karibia-nya Jawa ini, seperti rusa, kera, penyu, beragam
spesies burung, dan masih banyak lagi yang lainnya.</p>
<p>
  Sumber Artikel : <a href="https://www.traveloka.com/activities/indonesia">https://www.traveloka.com/activities/indonesia</a>
</p>
@endsection
