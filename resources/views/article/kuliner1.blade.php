@extends('culinary.details')
@section('navbar-kuliner')
  active
@endsection
@section('title')
Nasi Penggel, Sarapan Sederhana Khas Kebumen -
@endsection
@section('culinary-title')
Nasi Penggel, Sarapan Sederhana Khas Kebumen
@endsection
@section('culinary-image')
{{ asset('img/article/kuliner/nasi-penggel.jpg') }}
@endsection
@section('culinary-content')
<p>Sumber Gambar : <a href="https://assets.kompasiana.com/statics/files/142064615063413662.jp
g?t=o&v=700?t=o&v=700">https://assets.kompasiana.com/statics/files/142064615063413662.jp
g?t=o&v=700?t=o&v=700</a> </p>
<p>Kuliner nusantara takkan ada habisnya jika ingin ditelusuri.
Mengawali cerita, penulis membandingkan: Jika Pekalongan
memiliki Sego Megono, Surakarta mempunyai Nasi Liwet, Cirebon
memiliki Nasi Lengko, maka Kebumen mempunyai Nasi Penggel.
Sebutlah penggel seperti kita menyebut ‘e’ pada pensil. Jika berada
di Kebumen pagi hari, adalah sebuah keniscayaan untuk mengecap
rasa khas dari kuliner Kebumen yang memang hanya tersedia di
kala pagi ini. Selanjutnya, Anda bisa membayangkan bagaimana
penulis menggambarkan kuliner khas Kebumen ini. Ulasan menarik
yang kemudian membuat artikel ini banyak dibaca dan dikomentari
serta diganjar headline.</p>
@endsection
