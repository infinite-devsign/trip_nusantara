@extends('event.details')
@section('navbar-event')
  active
@endsection
@section('title')
  Festival Bau Nyale yang Menyenangkan 2018 di Pulau Lombok yang Memikat -
@endsection
@section('event-date')

@endsection
@section('event-title')
Festival Bau Nyale yang Menyenangkan 2018 di Pulau Lombok yang Memikat
@endsection
@section('event-image')
{{ asset('img/article/event/festifal-bau.jpg') }}
@endsection
@section('event-content')
<p>
  Sumber Gambar : <a href="www.scontent-sin6-2.xx.fbcdn.net">www.scontent-sin6-2.xx.fbcdn.net</a>
</p>
<p>Ingin menjadi bagian dari perayaan tradisional yang benar-benar menarik? Siapkan diri
Anda dengan baik! Bulan Februari mendatang, pulau Lombok yang mempesona akan
merayakan tradisi kuno Bau Nyale . Tradisi ini adalah salah satu festival rakyat tahunan
rakyat Sasak, di mana gerombolan orang berduyun-duyun ke laut untuk menangkap
cacing laut ( Nyale ). Menurut legenda setempat, Nyale mewakili nasib baik</p>
<p> <img src="{{ asset('img/article/event/festifal-bau2.jpg') }}" alt=""> </p>
<p> Sumber Gambar : <a href="www.garuda.industri.co.id">www.garuda.industri.co.id</a> </p>
<p>Festival tahun ini akan diadakan pada tanggal 28 Februari 2018 di tiga lokasi berbeda:
Pantai Kuta, Pantai Seger, dan Pantai Belanak. Tanggal yang tepat untuk festival
adalah hasil pertemuan di antara para tetua suku Sasak.
Dalam bahasa Lombok, Bau diterjemahkan sebagai "to-catch",
sedangkan Nyale adalah nama yang diberikan kepada cacing laut yang muncul sekali
setahun di sepanjang beberapa pantai terindah di Lombok.</p>
<p>Festival rakyat berawal dari legenda Putri Mandalika. Kembali di heydays raja dan ratu
Lombok, hiduplah seorang putri cantik bernama Mandalika. Dongeng dan desas-desus
tentang kecantikannya yang luar biasa dengan cepat menyebar ke seluruh pulau
sampai ke titik di mana pangeran di setiap sudut Lombok jatuh cinta padanya dan
sangat ingin menikahinya. Persaingan tak terhindarkan, dan gejolak akhirnya melanda
pulau itu.</p>
<p> <img src="{{ asset('img/article/event/festifal-bau3.jpg') }}" alt=""> </p>
<p> Sumber Gambar : <a href="www.cdns.klimg.com">www.cdns.klimg.com</a> </p>
<p>Melihat hal ini, sang putri sedih dan merindukan kedamaian untuk kembali ke
tanah. Dalam upaya putus asa untuk mengakhiri kekacauan sekali dan untuk
selamanya, Putri Mandalika melompat dari pantai dan melemparkan dirinya ke laut
terbuka. Orang-orangnya berusaha menyelamatkannya, tetapi alih-alih mengambil
kembali tubuhnya, mereka hanya menemukan banyak sekali cacing laut, yang
kemudian dikenal hari ini sebagai Nyale . Dengan demikian, Nyale diyakini sebagai
reinkarnasi dari Putri Mandalika yang dulu cantik.</p>
<p> <img src="{{ asset('img/article/event/festifal-bau4.jpg') }}" alt=""> </p>
<p> Sumber Gambar : <a href="www.scontent-sin6-2.xx.fbcdn.net">www.scontent-sin6-2.xx.fbcdn.net</a> </p>
<p>Sampai hari ini, Nyale muncul sekali setahun di pantai pantai Lombok, dan dianggap
sebagai reinkarnasi dari putri cantik yang pernah berkunjung ke bangsanya. Orangorang
juga percaya bahwa cacing adalah makhluk sakral yang membawa kemakmuran
bagi mereka yang menghormati mereka, atau malapetaka bagi mereka yang
mengabaikannya.</p>
<p> <img src="{{ asset('img/article/event/festifal-bau5.jpg') }}" alt=""> </p>
<p> Sumber Gambar : <a href="www.villabalisale.com">www.villabalisale.com</a> </p>
<p>Hari ini, festival telah berkembang menjadi lebih dari sekedar tradisi budaya, tetapi juga
dikombinasikan dengan atraksi menarik lainnya. Tahun ini, festival ini juga akan
menampilkan Pawai Budaya, Lomba Pembersih Pantai, Lomba Surfing dan Volley
Pantai, Kompetisi Selfie, Kontes Kecantikan Putri Mandalika, Kuliner Bazaar, dan
banyak lagi.</p>
<p> <img src="{{ asset('img/article/event/festifal-bau6.jpg') }}" alt=""> </p>
<p> Sumber Gambar : <a href="www.cdn2.tstatic.net">www.cdn2.tstatic.net</a> </p>
<p>"Kami bermaksud menghadirkan Festival Bau Nyale sebagai lebih dari sekedar upacara
budaya, tetapi lebih sebagai upaya serius dari Kabupaten Lombok Tengah dan Provinsi
Nusa Tenggara Barat untuk meningkatkan pariwisata di wilayah tersebut dan untuk
menarik lebih banyak wisatawan untuk mengunjungi pulau itu. bahwa Lombok pasti
memiliki beberapa pantai terindah di Indonesia, "kata Kepala Dinas Pariwisata Provinsi
Nusa Tenggara Barat, Lalu Mohammad Faozal.
Sementara itu, Menteri Pariwisata Arief Yahya menyampaikan apresiasinya kepada
Dinas Pariwisata Provinsi Nusa Tenggara Barat dan Lombok Tengah yang telah
mengubah festival rakyat menjadi acara yang inovatif dan menarik. "Ini sangat kreatif,
setiap tahun kami telah melihat perubahan progresif, dan acara ini disajikan lebih baik
dari tahun ke tahun," kata Menteri Arief Yahya.</p>
<p>
  Sumber Artikel : <a href="http://www.indonesia.travel/gb/en/event-festivals/the-exciting-bau-nyale-festival2018-in-the-enchanting-lombok-island">http://www.indonesia.travel/gb/en/event-festivals/the-exciting-bau-nyale-festival2018-in-the-enchanting-lombok-island</a>
</p>
@endsection
