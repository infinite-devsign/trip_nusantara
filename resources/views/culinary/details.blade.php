@extends('layouts.app')
@section('header')
  <link rel="stylesheet" href="{{ asset('css/details.css') }}">
  <style media="screen">
  .sidenav, .side-min {
    background: linear-gradient(to right, #d9a7c7, #fffcdc) !important;
  }
  </style>
@endsection
@section('nav-color')
  "linear-gradient(to right, #d9a7c7, #fffcdc)"
@endsection
@section('content')
@include('elements.navbar')
<div class="container">
  <div class="article-container">
    <div class="article">
      <div class="header">
        <div class="date-container">
          <div class="date">
            26<br>April<br>2016
          </div>
        </div>
        <div class="title">
          <h1>@yield('culinary-title')</h1>
        </div>
      </div>
      <div class="content">
        <div class="cover">
          <p><img src="@yield('culinary-image')" alt=""></p>
        </div>
        @yield('culinary-content')
      </div>
    </div>
  </div>
  <div class="widget-container">
    <div class="widget">
      <h3>Kuliner Lainnya</h3>
      <div class="list-destination">
        <div class="img-container">
          <img src="{{ asset('img/article/kuliner/nasi-penggel.jpg') }}" alt="">
        </div>
        <div class="name-container">
          <a href="{{ url('kuliner/nasi-penggel') }}">Nasi Penggel, Sarapan Sederhana Khas Kebumen</a>
        </div>
      </div>
      <div class="list-destination">
        <div class="img-container">
          <img src="{{ asset('img/article/kuliner/jajan-pasar.jpg') }}" alt="">
        </div>
        <div class="name-container">
          <a href="{{ url('kuliner/jajan-pasar') }}">Jajan Pasar: Kudapan Tradisional Cita Rasa Aduhai</a>
        </div>
      </div>
    </div>
  </div>

  <div class="review">
    <h3>Ulasan</h3>
    <div class="list-review-container">
      <p style="margin:20px">Belum ada ulasan.</p>
      {{-- <div class="list-review">
        <div class="user-img">
          <img src="{{ asset('img/Fariz.jpg') }}" alt="">
        </div>
        <div class="content-review">
          <h4>Nizar Alfarizi Akbar</h4>
          <div class="this-review">
            ga pernah naik gunung mas :D.
          </div>
        </div>
      </div>
      <div class="list-review">
        <div class="user-img">
          <img src="{{ asset('img/Fariz.jpg') }}" alt="">
        </div>
        <div class="content-review">
          <h4>Nizar Alfarizi Akbar</h4>
          <div class="this-review">
            ga pernah naik gunung mas :D.
          </div>
        </div>
      </div>
      <div class="list-review">
        <div class="user-img">
          <img src="{{ asset('img/Fariz.jpg') }}" alt="">
        </div>
        <div class="content-review">
          <h4>Nizar Alfarizi Akbar</h4>
          <div class="this-review">
            ga pernah naik gunung mas :D.
          </div>
        </div>
      </div> --}}
    </div>
  </div>

</div>
@endsection
