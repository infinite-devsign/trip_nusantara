@extends('culinary.details')
@section('title')
    {{ $culinary->title }} -
@endsection
@section('navbar-destinasi')
    active
@endsection
@section('culinary-title')
    {{ $culinary->title }}
@endsection
@section('culinary-image')
    {{ route('image', $culinary->media->first()->src) }}
@endsection
@section('culinary-content')
    <p>{{ $culinary->description }}</p>
    @if($culinary->media->count() > 1)
        <p>
            @foreach($culinary->media as $value)
                <img src="{{ route('image', $value->src) }}" alt="{{ $value->slug }}" style="width: 300px">
            @endforeach
        </p>
    @endif
@endsection