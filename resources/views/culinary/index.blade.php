@extends('layouts.app')
@section('header')
  <link rel="stylesheet" href="{{ asset('css/listPage.css') }}">
@endsection
@section('nav-color')
  "linear-gradient(to right, #d9a7c7, #fffcdc)"
@endsection
@section('navbar-kuliner')
  active
@endsection
@section('content')
  @include('elements.navbar')
  <div class="cover">
    <div class="filter"></div>
    <img src="{{ asset('img/article/kuliner/nasi-penggel.jpg') }}" alt="">
    <style media="screen">
      .title h3 a {
        text-decoration: none;
        color: black !important;
      }
      .description span:first-child {
        background: none;
      }
    </style>
  </div>
  <div class="header">
    <span>
      <h1>Kuliner</h1>
    </span>
    {{-- <a onclick="dropdown()" class="dropdown btn red">Test</a>
    <div id="myDropdown" class="dropdown-content">
      <a href="#home">Pengaturan</a>
      <a href="#about">Keluar</a>
    </div> --}}
  </div>
  <div class="content">
    @foreach($culinary as $value)
    <div class="box">
      <div class="img">
        <img class="lazyload" src="{{ route('image', $value->media->first()->src ) }}" data-original="{{ route('image', $value->media->first()->src ) }}" alt="{{ route('image', $value->media->first()->slug ) }}">
      </div>
      <div class="article">
        <div class="description">
          <span class="span1"></span>
          <span class="span2">Post On: {{ date('d-m-Y') }}</span>
          <span class="span3">0 Ulasan</span>
        </div>
        <div class="title">
          <h3><a href="{{ route('culinary.show', $value->slug) }}">{{ $value->title }}</a></h3>
        </div>
      </div>
    </div>
    @endforeach
  </div>
@endsection
