<!DOCTYPE html>
<html lang="en">
<head>
  @include('elements/header')
  <link rel="stylesheet" href="{{ asset('css/admin.css') }}">
  @yield('header')
</head>
<body>
@include('member.layouts.partials.navbar')
@yield('content')

@include('elements/scripts')
@yield('scripts')
</body>
</html>
