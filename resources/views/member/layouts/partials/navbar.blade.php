<nav>
  <ul class="sidenav">
    <li><a class="" href="{{ route('member.') }}">Dasbor</a></li>
    <li><a class="" href="{{ route('member.destinasi') }}">Destinasi</a></li>
    <li><a class="" href="{{ route('member.galeri') }}">Galeri</a></li>
    <li><a class="" href="{{ route('member.kuliner') }}">Kuliner</a></li>
    <li><a class="" href="{{ route('member.event') }}">Event</a></li>
  </ul>
  <div class="backnav"></div>
  <ul class="side-min">
    <li><a class="navbar btn-action">&#9776;</a></li>
    <li><a href="#">Logo</a></li>
    <li>
      <a onclick="dropdown()" class="user btn-action"><img class="img-user" alt=""
          @if(Auth()->user()->media()->count() == 0)
              src="{{ asset('img/avatar.png') }}"
          @else
              src="{{ route('image', Auth()->user()->media->first()->src) }}"
          @endif
          >
      </a>
    </li>
  </ul>
  <div id="myDropdown" class="dropdown-content">
    <span class="blue">{{ Auth()->user()->name }}</span>
    <a href="{{ route('member.setting') }}">Pengaturan</a>
    <a href="{{ route('member.logout') }}">Keluar</a>
  </div>
</nav>
