<?php
/**
 * Created by PhpStorm.
 * User: ANDROMEDA
 * Date: 4/3/2018
 * Time: 8:32 PM
 */
?>
@extends('member.layouts.app')
@section('content')
    <div class="container">
        <div class="content" style="float:left; padding: 0; width:100%; padding: 0px 4px 20px 0px;">
            <h2 style="margin-left:22px">Buat Galeri</h2>
            @foreach($errors->all() as $error)
                <div class="notification">
                    <div class="alert alert-danger">
                        {{ $error }}
                    </div>
                </div>
            @endforeach
            <form class="form" action="{{ Route('member.galeri.store') }}" method="post" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="group">
                    <label for="title">Judul <span class="required">*</span></label>
                    <input id="title" class="input-field input-field-long" type="text" name="title" required>
                    <label for="description">Deskripsi <span class="required">*</span></label>
                    <textarea name="description" id="description" class="input-field input-field-long"
                              required></textarea>
                </div>
                <div class="group-full">
                    <label for="images.0">Gambar <span class="required">*</span></label>
                    <input id="images.0" class="input-field" type="file" name="images[]" multiple required>
                </div>
                <div class="group-full" style="text-align:center;">
                    <button class="btn green" type="submit" style="margin-top:12px;">Simpan</button>
                </div>
            </form>
        </div>
    </div>
@endsection
