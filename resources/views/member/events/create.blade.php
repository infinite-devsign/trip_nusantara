<?php
/**
 * Created by PhpStorm.
 * User: ANDROMEDA
 * Date: 4/3/2018
 * Time: 8:32 PM
 */
?>
@extends('member.layouts.app')
@section('content')
    <div class="container">
        <div class="content" style="float:left; padding: 0; width:100%">
            <h2 style="margin-left:22px">Buat Event</h2>
            @foreach($errors->all() as $error)
                <div class="notification">
                    <div class="alert alert-danger">
                        {{ $error }}
                    </div>
                </div>
            @endforeach
            <form class="form" action="{{ Route('member.event.store') }}" method="post" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="group">
                    <label for="title">Judul <span class="required">*</span></label>
                    <input id="title" class="input-field input-field-long" type="text" name="title" required>
                    <label for="images.0">Gambar <span class="required">*</span></label>
                    <input id="images.0" class="input-field" type="file" name="images[]" multiple required>
                </div>
                <div class="group">
                    <label for="date_start.">Tanggal Mulai <span class="required">*</span></label>
                    <select name="date_start[]" id="date_start." style="color: #000;">
                        <option value="">Tanggal</option>
                        @for($d=1; $d<=31; $d++)
                            <option value="{{ $d }}">
                                {{ $d }}
                            </option>
                        @endfor
                    </select>
                    <select name="date_start[]" id="date_start." style="color: #000;">
                        <option value="">Bulan</option>
                        @for($m=0; $m<=11; $m++)
                            <option value="{{ date('F', strtotime("$m month"))}}">
                                {{ date('F', strtotime("+$m month"))}}
                            </option>
                        @endfor
                    </select>
                    <select name="date_start[]" id="date_start." style="color: #000;">
                        <option value="">Tahun</option>
                        @for($y=0; $y<=5; $y++)
                            <option value="{{ date('Y', strtotime("last day of +$y year")) }}">
                                {{ date('Y', strtotime("last day of +$y year")) }}
                            </option>
                        @endfor
                    </select>
                </div>
                <div class="group-full">
                  <label for="description">Deskripsi <span class="required">*</span></label>
                  <textarea name="description" id="description" class="input-field input-field-long"
                            required></textarea>
                </div>
                <div class="group-full" style="text-align: center; margin-top:12px;">
                    <button class="btn green" type="submit" style="text-align: center">Save</button>
                </div>
            </form>
        </div>
    </div>
@endsection
