<?php
/**
 * Created by PhpStorm.
 * User: ANDROMEDA
 * Date: 4/3/2018
 * Time: 8:32 PM
 */
?>
@extends('member.layouts.app')
@section('content')
    <div class="container">
        <div class="content" style="float:left; padding: 0; width:100%">
            <h2 style="margin-left:22px">Edit Event</h2>
            @foreach($errors->all() as $error)
                <div class="notification">
                    <div class="alert alert-danger">
                        {{ $error }}
                    </div>
                </div>
            @endforeach
            <form class="form" action="{{ Route('member.event.update', $event->slug) }}" method="post"
                  enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="group">
                    <label for="title">Judul <span class="required">*</span></label>
                    <input id="title" class="input-field input-field-long" type="text" name="title"
                           value="{{ $event->title }}" required>
                    <label for="description">Deskripsi <span class="required">*</span></label>
                    <textarea name="description" id="description" class="input-field input-field-long"
                              required>{{ $event->description }}</textarea>
                </div>
                <div class="group">
                    <select name="date_start[]" id="date_start." style="color: #000;">
                        <option value="">Tanggal</option>
                        @for($d=1; $d<=31; $d++)
                            <option value="{{ $d }}"
                                @if($d == $date[0])
                                    {{ "SELECTED" }}
                                @endif
                            >
                                {{ $d }}
                            </option>
                        @endfor
                    </select>
                    <select name="date_start[]" id="date_start." style="color: #000;">
                        <option value="">Bulan</option>
                        @for($m=0; $m<=11; $m++)
                            <option value="{{ date('F', strtotime("+$m month")) }}"
                                @if(date('F', strtotime("+$m month")) == $date[1])
                                    {{ "SELECTED" }}
                                @endif
                            >
                                {{ date('F', strtotime("+$m month"))}}
                            </option>
                        @endfor
                    </select>
                    <select name="date_start[]" id="date_start." style="color: #000;">
                        <option value="">Tahun</option>
                        @for($y=0; $y<=5; $y++)
                            <option value="{{ date('Y', strtotime("last day of +$y year")) }}"
                                @if(date('Y', strtotime("last day of +$y year")) == $date[2])
                                    {{ "SELECTED" }}
                                @endif
                            >
                                {{ date('Y', strtotime("last day of +$y year")) }}
                            </option>
                        @endfor
                    </select>
                </div>
                <div class="group">
                    <button class="btn green" type="submit" style="text-align: center">Save</button>
                </div>
            </form>
        </div>
    </div>
@endsection
