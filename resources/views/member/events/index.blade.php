<?php
/**
 * Created by PhpStorm.
 * User: ANDROMEDA
 * Date: 4/3/2018
 * Time: 8:25 PM
 */
?>
@extends('member.layouts.app')
@section('content')
    <div class="container">
        <div class="content content-table">
            @if(session('success'))
                <div class="notification">
                    <div class="alert alert-success">
                        {{ session('success') }}
                    </div>
                </div>
            @endif
            <div class="top">
                <span style="float:left"><h1>Event</h1></span>
                <a href="{{ route('member.event.create') }}" class="btn btn-small blue"
                   style="margin: 20px; float:left">Tambah</a>
            </div>
            <table class="table">
                <tr>
                    <th>#</th>
                    <th>Title</th>
                    <th>Status</th>
                    <th>Author</th>
                    <th>Gambar</th>
                    <th>Aksi</th>
                </tr>
                @foreach($event as $key => $value)
                    <tr>
                        <td>{{ $key+1 }}</td>
                        <td>{{ $value->title }}</td>
                        <td>
                            @if($value->slug == 'published')
                                <span class="badge badge-success">Published</span>
                            @else
                                <span class="badge badge-warning">Pending</span>
                            @endif
                        </td>
                        <td>{{ $value->user->name }}</td>
                        <td>{{ $value->media()->count() }}</td>
                        <td width="255">
                            <a href="{{ route('member.event.edit', $value->slug) }}" class="btn blue"
                               style="float:left">Edit</a>
                            <a href="#" class="btn green" style="float:left; margin-left:5px">Lihat</a>
                            <span style="float:left; margin-left:5px">
                          <form class="" action="{{ route('member.event.delete', $value->slug) }}" method="post">
                              {{ csrf_field() }}
                              {{ method_field('DELETE') }}
                              <button type="submit" class="btn red">Hapus</button>
                          </form>
                        </span>
                        </td>
                    </tr>
                @endforeach
            </table>
            {{ $event->links('vendor.pagination.paging') }}
        </div>
    </div>
@endsection

