@extends('member.layouts.app')
@section('header')
@endsection
@section('content')
    <div class="container">
        @if (session('status'))
            <div class="notification">
                <div class="alert alert-success">
                    {{ session('status') }}
                </div>
            </div>
        @endif
        <div class="count-box">
            <div class="top">
                {{ $destination }}
            </div>
            <div class="bottom blue">
                Destinasi
                <a href="{{ route('member.destinasi.create') }}" class="btn white">Tambah</a>
                <a href="{{ route('member.destinasi') }}" class="btn white">List</a>
            </div>
        </div>
        <div class="count-box">
            <div class="top">
                {{ $gallery }}
            </div>
            <div class="bottom green">
                Galeri
                <a href="{{ route('member.galeri.create') }}" class="btn white">Tambah</a>
                <a href="{{ route('member.galeri') }}" class="btn white">List</a>
            </div>
        </div>
        <div class="count-box">
            <div class="top">
                {{ $culinary }}
            </div>
            <div class="bottom red">
                Kuliner
                <a href="{{ route('member.kuliner.create') }}" class="btn white">Tambah</a>
                <a href="{{ route('member.kuliner') }}" class="btn white">List</a>
            </div>
        </div>
        <div class="count-box">
            <div class="top">
                {{ $event }}
            </div>
            <div class="bottom red">
                Event
                <a href="{{ route('member.event.create') }}" class="btn white">Tambah</a>
                <a href="{{ route('member.event') }}" class="btn white">List</a>
            </div>
        </div>
    </div>
@endsection
