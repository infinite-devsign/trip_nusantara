@extends('layouts.app')
@section('navbar-destinasi')
    active
@endsection
@section('nav-color')
    "linear-gradient(to right, #74ebd5, #acb6e5)"
@endsection
@section('header')
    <link rel="stylesheet" href="{{ asset('css/listPage.css') }}">
    <style media="screen">
        .title h3 a {
            text-decoration: none;
            color: black !important;
        }
    </style>
@endsection
@section('content')
    @include('elements.navbar')
    <div class="cover">
        <div class="filter"></div>
        <img src="{{ asset('img/article/destination/nusa-dua.jpg') }}" alt="">
    </div>
    <div class="header">
    <span>
      <h1>Destinasi</h1>
    </span>
    </div>
    <div class="content">
        @foreach($destination as $value)
            <div class="box">
                <div class="img">
                    <img class="lazyload" src="{{ route('image', $value->media->first()->src) }}"
                         data-original="{{ route('image', $value->media->first()->src)}}">
                </div>
                <div class="article">
                    <div class="description">
                        <span style="background:none;"></span>
                        <span>{{ $value->title }}</span>
                        <span>0 Ulasan</span>
                    </div>
                    <div class="title">
                        <h3><a href="{{ route('destination.show', $value->slug) }}">{{ $value->title }}</a></h3>
                    </div>
                </div>
            </div>
        @endforeach
    </div>
@endsection
