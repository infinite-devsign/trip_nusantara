@extends('layouts.app')
@section('header')
    <link rel="stylesheet" href="{{ asset('css/destination.css') }}">
@endsection
@section('nav-color')
    "linear-gradient(to right, #74ebd5, #acb6e5)"
@endsection
@section('content')
    @include('elements.navbar')
    <div class="cover">
        <div class="filter"></div>
        <img src="@yield('image-destination')" alt="">
    </div>
    <div class="header">
    <span class="title">
      <h1>@yield('title-destination')</h1>
    </span>
        <div class="rate">
            <p>Rate: </p> <img src="{{ asset('img/star/5.png') }}" alt="">
        </div>
    </div>
    <div class="content">
        <div class="detail">
            <div>
                @yield('destination')
            </div>
        </div>
        <div class="more-destination">
            <div class="more-container">
                <h3>Destinasi Lainnya</h3>
                <div class="list-destination">
                    <div class="img-container">
                        <img src="{{ asset('img/article/destination/danau-toba.jpg') }}" alt="">
                    </div>
                    <div class="name-container">
                        <a href="{{ url('destinasi/danau-toba') }}">Danau Toba</a>
                    </div>
                </div>
                <div class="list-destination">
                    <div class="img-container">
                        <img src="{{ asset('img/article/destination/bromo.jpg') }}" alt="">
                    </div>
                    <div class="name-container">
                        <a href="{{ url('destinasi/gunung-bromo') }}">Gunung Bromo</a>
                    </div>
                </div>
                <div class="list-destination">
                    <div class="img-container">
                        <img src="{{ asset('img/article/destination/karimun-jawa.jpg') }}" alt="">
                    </div>
                    <div class="name-container">
                        <a href="{{ url('destinasi/karimun-jawa') }}">Kepulauan Karimun Jawa </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="map" id="map"></div>
    <div class="review">
        <h3>Ulasan</h3>
        <div class="list-review-container">
            <p style="margin: 30px">belum ada ulasan.</p>
            {{-- <div class="list-review">
              <div class="user-img">
                <img src="{{ asset('img/ea.jpg') }}" alt="">
              </div>
              <div class="content-review">
                <h4>Nizar Alfarizi Akbar</h4>
                <div class="this-review">
                  ga pernah naik gunung mas :D.
                </div>
              </div>
            </div>
            <div class="list-review">
              <div class="user-img">
                <img src="{{ asset('img/ea.jpg') }}" alt="">
              </div>
              <div class="content-review">
                <h4>Nizar Alfarizi Akbar</h4>
                <div class="this-review">
                  ga pernah naik gunung mas :D.
                </div>
              </div>
            </div>
            <div class="list-review">
              <div class="user-img">
                <img src="{{ asset('img/ea.jpg') }}" alt="">
              </div>
              <div class="content-review">
                <h4>Nizar Alfarizi Akbar</h4>
                <div class="this-review">
                  ga pernah naik gunung mas :D.
                </div>
              </div>
            </div> --}}
        </div>
    </div>
@endsection
@section('scripts')
    <script>
        $('.owl-carousel').owlCarousel({
            loop:true,
            margin:10,
            responsiveClass:true,
            mouseDrag:true,
            autoplay:true,
            autoplayHoverPause:true,
            responsive:{
                0:{
                    items:1,
                    nav:true
                },
                600:{
                    items:3,
                    nav:false
                },
                1000:{
                    items:5,
                    nav:true,
                    loop:false
                }
            }
        })
    </script>
@endsection