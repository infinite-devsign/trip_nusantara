@extends('destination.details')
@section('title')
    {{ $destination->title }} -
@endsection
@section('navbar-destinasi')
    active
@endsection
@section('title-destination')
    {{ $destination->title }}
@endsection
@section('image-destination')
    {{ route('image', $destination->media->first()->src) }}
@endsection
@section('destination')
    <p>{{ $destination->description }}</p>
    @if($destination->media->count() > 1)
        <p>
            @foreach($destination->media as $value)
                <img src="{{ route('image', $value->src) }}" alt="{{ $value->slug }}" style="width: 300px">
            @endforeach
        </p>
    @endif
@endsection
@section('scripts')
    <script>
        function initMap() {
            var lat = JSON.parse("{{ (double)$position[0] }}");
            var lng = JSON.parse("{{ (double)$position[1] }}");
            console.log(lat, lng);
            var position = {lat: lat, lng: lng};
            var map = new google.maps.Map(document.getElementById('map'), {
                zoom: 14,
                center: position
            });
            var marker = new google.maps.Marker({
                position: position,
                map: map
            });
        }
    </script>
@endsection