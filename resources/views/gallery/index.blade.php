@extends('layouts.app')
@section('header')
  <link rel="stylesheet" href="{{ asset('css/gallery.css') }}">
  <style media="screen">
  .sidenav, .side-min {
    background: linear-gradient(to right, #232526, #414345) !important;
  }
  .sidenav li a, .side-min li a, .sidenav li a.active {
    color: white !important;
  }
  .sidenav li a.active {
    border-bottom: white 3px solid !important;
  }
  @media screen and (max-width: 900px) {
    .sidenav li a, .sidenav li a.active {
      color: black !important;
    }
  }
  </style>
@endsection
@section('navbar-galeri')
  active
@endsection
@section('nav-color')
  "linear-gradient(to right, #34e89e, #0f3443)"
@endsection
@section('content')
  @include('elements.navbar')
  <div class="container">
    <div class="content">
      @foreach($gallery as $value)
        <div class="box">
          <div class="content-container">
            <div class="img">
              <img class="lazyload" src="{{ route('image', $value->media->first()->src) }}" data-original="{{ route('image', $value->media->first()->src) }}" alt="">
            </div>
          </div>
          <div class="content-container">
            <div class="description">
              <a href="{{ route('gallery.show', $value->slug) }}">{{ $value->title }}</a>
            </div>
          </div>
        </div>
      @endforeach
    </div>
  </div>
@endsection
