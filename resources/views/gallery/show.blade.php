@extends('gallery.details')
@section('navbar-galeri')
  active
@endsection
@section('title')
{{ $gallery->title }} -
@endsection
@section('gallery-title')
  {{ $gallery->title }}
@endsection
@section('gallery-content')
<p>{{ $gallery->description }}</p>
<img src="{{ route('image', $gallery->media->first()->src ) }}" alt="">
@endsection
