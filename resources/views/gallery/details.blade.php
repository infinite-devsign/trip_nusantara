@extends('layouts.app')
@section('header')
  <link rel="stylesheet" href="{{ asset('css/gallery.css') }}">
  <style media="screen">
    .container-images {
        float: left;
        margin-top: 20px;
    }
    .container-images p {
        float: left;
        margin-left: 20px;
    }
    .sidenav, .side-min {
      background: linear-gradient(to right, #232526, #414345) !important;
    }
    .sidenav li a, .side-min li a, .sidenav li a.active {
      color: white !important;
    }
    .sidenav li a.active {
      border-bottom: white 3px solid !important;
    }
  </style>
@endsection
@section('nav-color')
  "linear-gradient(to right, #232526, #414345)"
@endsection
@section('content')
@include('elements.navbar')
<div class="container">
  <div class="content-details">
    <div class="images">
      <div class="title">
        <h1>@yield('gallery-title')</h1>
      </div>
      <div class="container-images">
          @yield('gallery-content')
      </div>
    </div>
    <div class="images-description">
      <div class="description-container">
        <div class="name-author">
          <p>Erik Prayoga</p>
        </div>
        <div class="avatar-author">
          <img src="{{ asset('img/Erik.jpg') }}" alt="">
        </div>
      </div>
      <div class="review">
        <h3>Komentar</h3>
        <div class="list-review-container">
          <p style="margin:20px">Tidak ada Komentar.</p>
          {{-- <div class="list-review">
            <div class="user-img">
              <img src="{{ asset('img/Fariz.jpg') }}" alt="">
            </div>
            <div class="content-review">
              <h4>Nizar Alfarizi Akbar</h4>
              <div class="this-review">
                ga pernah naik gunung mas :D.
              </div>
            </div>
          </div>
          <div class="list-review">
            <div class="user-img">
              <img src="{{ asset('img/Fariz.jpg') }}" alt="">
            </div>
            <div class="content-review">
              <h4>Nizar Alfarizi Akbar</h4>
              <div class="this-review">
                ga pernah naik gunung mas :D.
              </div>
            </div>
          </div>
          <div class="list-review">
            <div class="user-img">
              <img src="{{ asset('img/Fariz.jpg') }}" alt="">
            </div>
            <div class="content-review">
              <h4>Nizar Alfarizi Akbar</h4>
              <div class="this-review">
                ga pernah naik gunung mas :D.
              </div>
            </div>
          </div> --}}
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
