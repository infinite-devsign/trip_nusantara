<footer class="footer">
    <div class="footer-content">
        <div class="sub">
            <h1>Liwung</h1>
            <h3>Laman Jalan-Jalan Lintas Nusantara.</h3>
            <p>Laman Pariwisata untuk lomba yang kreatif, inovatif, serta merupakan karya anak SMK, Liwung bermakna
                jalan-jalan santai yang tidak memiliki tujuan awal yang jelas.</p>
        </div>
        <div class="sub">
            <h3>Make With Love in Everywhere</h3>
            <p>Terima kasih kepada VOCOMVEST 2018 yang telah menjadi wadah untuk para desainer laman web yang duduk
                dibangku sekolah.</p>
            <p>Suported By IT CLUB SMKN 1 Surabaya</p>
            <p>Copyright (C) 2018 - INFINITE DEVSIGN</p>
        </div>
        <div class="sub">
            <h1>About Us</h1>
            <div class="anggota">
                <a><img src="{{ asset('img/Rizky.png') }}" alt=""></a>
                <h2>Rizky</h2>
                <h3>Backend Programmer</h3>
            </div>
            <div class="anggota">
                <a><img src="{{ asset('img/Fariz.jpg') }}" alt=""></a>
                <h2>Nizar</h2>
                <h3>Frontend Programmer</h3>
            </div>
            <div class="anggota">
                <a><img src="{{ asset('img/Erik.jpg') }}" alt=""></a>
                <h2>Erik</h2>
                <h3>UI/UX Designer</h3>
            </div>
        </div>
    </div>
</footer>
