<script type="text/javascript" src="{{ asset('js/jquery-3.2.1.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/jquery.lazyload.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/app.js') }}"></script>
<script type="text/javascript">
$(document).ready(function(){
  var firstPosition = $(window).scrollTop();
  scrollEffects();
  var firstPosition = null;
  $(window).scroll(function (event) {
    var scroll = $(window).scrollTop();
    // console.log(scroll);
    scrollEffects(scroll);
  });
  function scrollEffects(scroll) {
    if (scroll > 50 || firstPosition > 50) {
      $(".sidenav, .side-min").css("background", @yield('nav-color'));
      $(".sidenav li a, .side-min li a").css("color", "black");
      $(".sidenav, .side-min").css("box-shadow", "0px 10px 32px -5px rgba(0,0,0,0.1)");
      $("ul.sidenav li a.active, ul.side-min li a.active").css("border-bottom", "3px solid black");
    } else {
      $(".sidenav, .side-min").css("background", "transparent");
      $(".sidenav li a, .side-min li a").css("color", "white");
      $(".sidenav, .side-min").css("box-shadow", "none");
      $("ul.sidenav li a.active, ul.side-min li a.active").css("border-bottom", "3px solid transparent");
    }
  }
});
window.addEventListener("load", function(event) {
  lazyload();
});
$(".lazyload").lazyload();
</script>
