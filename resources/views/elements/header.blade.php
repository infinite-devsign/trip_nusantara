<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta content='text/html; charset=UTF-8' http-equiv='Content-Type'/>

<!-- CSRF Token -->
<meta name="google-site-verification" content="TSEB9PwVdw92DWqoQfbRz8ScP2qfzSaLKajSI-9Rgwc" />
<meta name="csrf-token" content="{{ csrf_token() }}">
<title>{{ $title or 'Liwung Indonesia' }}</title>

<!-- Favicon -->
<link href="{{ asset('favicon.ico') }}" rel="icon" type="image/x-icon">

<!-- SEO -->
<meta name="description" content="{{ $description or 'Liwung. Laman Jalan-Jalan Lintas Nusantara. Laman Pariwisata untuk lomba yang kreatif, inovatif, serta merupakan karya anak SMK, Liwung bermakna jalan-jalan santai yang tidak memiliki tujuan awal yang jelas.'}}">
<meta name="keywords" content="{{ $keywords or  'Tempatnya berbagi info mengenai tempat wisata, Kuliner, dan Event-event yang ada di Indonesia' }}">

<meta content='Indonesia' name='geo.placename'/>
<meta content='general' name='rating'/>
<meta content='id' name='geo.country'/>

<meta name="twitter:card" content="summary_large_image">
<meta name="twitter:title" content="{{ $title or 'Liwung Indonesia' }}">
<meta name="twitter:description" content="{{ $description or 'Liwung. Laman Jalan-Jalan Lintas Nusantara. Laman Pariwisata untuk lomba yang kreatif, inovatif, serta merupakan karya anak SMK, Liwung bermakna jalan-jalan santai yang tidak memiliki tujuan awal yang jelas.'}}">
<meta name="twitter:image" content="{{ $image or asset('img/logo.png') }}">

<meta property="og:title" content="{{ $title or 'Liwung Indonesia' }}" />
<meta property="og:type" content="website" />
<meta property="og:url" content="{{ url()->current() }}" />
<meta property="og:image" content="{{ $image or asset('img/logo.png') }}" />
<meta property="og:description" content="{{ $description or 'Liwung. Laman Jalan-Jalan Lintas Nusantara. Laman Pariwisata untuk lomba yang kreatif, inovatif, serta merupakan karya anak SMK, Liwung bermakna jalan-jalan santai yang tidak memiliki tujuan awal yang jelas.'}}" />
<meta property="og:site_name" content="Liwung Indonesia" />
<meta content='id_ID' property="og:locale">
<meta content='en_US' property="og:locale:alternate">

<link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet">
<link rel="stylesheet" href="{{ asset('css/fa-regular.min.css') }}">
<link rel="stylesheet" href="{{ asset('css/app.css') }}">
<link rel="stylesheet" href="{{ asset('css/animation.css') }}">