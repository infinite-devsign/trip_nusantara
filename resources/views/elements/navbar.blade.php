{{-- <form class="" action="index.php" method="post">
  <input type="text" name="search" class="search-box" placeholder="Type and hit enter">
</form> --}}
<div class="search-container"></div>
<nav>
    <ul class="sidenav">
        <li><a class="@yield('navbar-beranda')" href="{{ url('/') }}">Beranda</a></li>
        <li><a class="@yield('navbar-destinasi')" href="{{ url('/destinasi') }}">Destinasi</a></li>
        <li><a class="@yield('navbar-event')" href="{{ url('/event') }}">Event</a></li>
        <li><a class="@yield('navbar-kuliner')" href="{{ url('/kuliner') }}">Kuliner</a></li>
        <li><a class="@yield('navbar-galeri')" href="{{ url('/galeri') }}">Galeri</a></li>
        <li>
            {{-- <a class="search-button search-hidden btn-action">Cari</a> --}}
            @if (Auth()->user())
                <a onclick="dropdown()" class="user btn-action"><img class="img-user" alt=""
                                                                     @if(Auth()->user()->media()->count() == 0)
                                                                     src="{{ asset('img/avatar.png') }}"
                                                                     @else
                                                                     src="{{ route('image', Auth()->user()->media->first()->src) }}"
                            @endif
                    >
                </a>
            @else
                <a onclick="dropdown()" class="user btn-action"><img class="img-user" alt=""
                                                                     src="{{ asset('img/avatar.png') }}">
                </a>
            @endif
        </li>
    </ul>
    <div class="backnav"></div>
    <ul class="side-min">
        <li><a class="navbar btn-action">&#9776;</a></li>
        <li><a href="{{ route('index') }}"><img src="{{ asset('img/logo.png') }}" alt=""> </a></li>
        <li>
            {{-- <a class="search-button btn-action">Cari</a> --}}
            @if (Auth()->user())
                <a onclick="dropdown()" class="user btn-action"><img class="img-user" alt=""
                                                                     @if(Auth()->user()->media()->count() == 0)
                                                                     src="{{ asset('img/avatar.png') }}"
                                                                     @else
                                                                     src="{{ route('image', Auth()->user()->media->first()->src) }}"
                            @endif
                    >
                </a>
            @else
                <a onclick="dropdown()" class="user btn-action"><img class="img-user" alt=""
                                                                     src="{{ asset('img/avatar.png') }}">
                </a>
            @endif
        </li>
    </ul>
    <div id="myDropdown" class="dropdown-content dropdown-user">
        @if (!Auth()->user())
            <a href="{{ route('login') }}">Masuk</a>
            <a href="{{ route('register') }}">Registrasi</a>
        @elseif (Auth::user()->role == 'admin')
            <span class="blue">{{ Auth()->user()->name }}</span>
            <a href="{{ route('admin.') }}">Dashboard</a>
            <a href="{{ route('admin.setting') }}">Pengaturan</a>
            <a href="{{ route('admin.logout') }}">Keluar</a>
        @elseif(Auth()->user()->role == 'member')
            <span class="blue">{{ Auth()->user()->name }}</span>
            <a href="{{ route('member.') }}">Dashboard</a>
            <a href="{{ route('member.setting') }}">Pengaturan</a>
            <a href="{{ route('member.logout') }}">Keluar</a>
        @endif
    </div>
</nav>
