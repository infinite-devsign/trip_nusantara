@extends('admin.layouts.app')
@section('header')
    <style media="screen">
        .container {
            margin-top: 60px;
        }

        .box {
            position: absolute;
            left: 0;
            right: 0;
            margin-left: auto;
            margin-right: auto;
            width: 600px;
            height: auto;
            background: white;
        }

        .box h1 {
            text-align: center;
            margin: 0;
            padding: 12px 0px;
        }

        .avatar img {
            width: 50%;
            margin: 20px 25%;
            height: auto;
            border-radius: 999px;
        }

        .info-body {
            padding: 0 30px;
        }
    </style>
@endsection
@section('content')
    <div class="container">
        <div class="box">
            <h1 class="blue">Pengaturan Akun</h1>
            @if(session('success'))
                <div class="notification" style="margin: auto;">
                    <div class="alert alert-success">
                        {{ session('success') }}
                    </div>
                </div>
            @endif
            <div class="avatar">
                <img
                        @if($user->media()->count() == 0)
                            src="{{ asset('img/avatar.png') }}"
                        @else
                            src="{{ route('image', $user->media->first()->src) }}"
                        @endif
                >
                <form class="form" action="{{ route('admin.setting.avatar') }}" method="post" style="width: 50%; margin: 0px 25%; text-align:center" enctype="multipart/form-data">
                    @csrf
                    <label for="avatar">Ganti Avatar</label>
                    <input id="avatar"
                           class="input-field input-field-long {{ $errors->has('avatar') ? ' is-invalid' : '' }}"
                           type="file" name="avatar[]" value="{{ old('avatar') }}" required autofocus>
                    @if ($errors->has('avatar'))
                        <span class="invalid-feedback">
                <strong>{{ $errors->first('avatar') }}</strong>
            </span>
                    @endif
                    <button class="btn blue" type="submit">Submit</button>
                </form>
            </div>
            <div class="info-body">
                <form class="form" method="post" action="{{ route('admin.setting.update') }}">
                    @csrf
                    <label for="name">Nama</label>
                    <input id="name"
                           class="input-field input-field-long {{ $errors->has('name') ? ' is-invalid' : '' }}"
                           type="text" name="name" value="{{ $user->name or old('name') }}" required autofocus>
                    @if ($errors->has('name'))
                        <span class="invalid-feedback">
                <strong>{{ $errors->first('name') }}</strong>
            </span>
                    @endif
                    <label for="username">Username</label>
                    <input id="username"
                           class="input-field input-field-long {{ $errors->has('username') ? ' is-invalid' : '' }}"
                           type="text" name="username" value="{{ $user->username or old('username') }}" required>
                    @if ($errors->has('username'))
                        <span class="invalid-feedback">
                <strong>{{ $errors->first('username') }}</strong>
            </span>
                    @endif
                    <label for="email">Email</label>
                    <input id="email"
                           class="input-field input-field-long {{ $errors->has('email') ? ' is-invalid' : '' }}"
                           type="email" name="email" value="{{ $user->email or old('email') }}" required>
                    @if ($errors->has('email'))
                        <span class="invalid-feedback">
                <strong>{{ $errors->first('email') }}</strong>
            </span>
                    @endif
                    <label for="password">Password</label>
                    <input id="password"
                           class="input-field input-field-long {{ $errors->has('password') ? ' is-invalid' : '' }}"
                           type="password" name="password" value="{{ old('password') }}" required>
                    @if ($errors->has('password'))
                        <span class="invalid-feedback">
                <strong>{{ $errors->first('password') }}</strong>
            </span>
                    @endif
                    <button class="btn green" type="submit">Submit</button>
                </form>
            </div>
        </div>
    </div>
@endsection
