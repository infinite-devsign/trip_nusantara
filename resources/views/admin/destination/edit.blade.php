<?php
/**
 * Created by PhpStorm.
 * User: ANDROMEDA
 * Date: 3/27/2018
 * Time: 10:40 AM
 */

?>

@extends('admin.layouts.app')
@section('content')
    <div class="container">
        <div class="content" style="float:left; padding: 0; width:100%">
            <h2 style="margin-left:22px">Edit Destinasi</h2>
            @foreach($errors->all() as $error)
                <div class="notification">
                    <div class="alert alert-danger">
                        {{ $error }}
                    </div>
                </div>
            @endforeach
            <form class="form" action="{{ Route('admin.destinasi.update', $destination->slug) }}" method="post">
                {{ csrf_field() }}
                <div class="group">
                    <label for="title">Judul <span class="required">*</span></label>
                    <input id="title" class="input-field input-field-long" type="text" name="title"
                           value="{{ $destination->title }}" required>
                    <label for="description">Deskripsi <span class="required">*</span></label>
                    <textarea name="description" id="description" class="input-field input-field-long"
                              required>{{ $destination->description }}</textarea>
                </div>
                <div class="group">
                    <label for="location">Lokasi <span class="required">*</span></label>
                    <input id="location" class="input-field input-field-long" type="text"
                           value="{{ $destination->location }}" name="location" required>
                </div>
                <div class="group">
                    <button class="btn green" type="submit" style="text-align: center">Edit</button>
                </div>
            </form>
        </div>
    </div>
@endsection

