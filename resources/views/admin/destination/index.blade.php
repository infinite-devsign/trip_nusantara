@extends('admin.layouts.app')
@section('content')
    <div class="container">
        <div class="content content-table">
            @if(session('success'))
                <div class="notification">
                    <div class="alert alert-success">
                        {{ session('success') }}
                    </div>
                </div>
            @endif
            <div class="top">
                <span style="float:left"><h1>Destinasi</h1></span>
                <a href="{{ route('admin.destinasi.create') }}" class="btn btn-small blue"
                   style="margin: 20px; float:left">Tambah</a>
            </div>
            <table class="table">
                <tr>
                    <th>#</th>
                    <th>Title</th>
                    <th>Status</th>
                    <th>Author</th>
                    <th>Gambar</th>
                    <th>Aksi</th>
                </tr>
                @foreach($destination as $key => $value)
                    <tr>
                        <td>{{ $key+1 }}</td>
                        <td>{{ $value->title }}</td>
                        <td>
                            <button id="status" value="{{ $value->slug }}"> {{ $value->status }}</button>
                        </td>
                        <td>{{ $value->user->name }}</td>
                        <td>{{ $value->media()->count() }}</td>
                        <td width="255">
                            <a href="{{ route('admin.destinasi.edit', $value->slug) }}" class="btn blue"
                               style="float:left">Edit</a>
                            <a href="#" class="btn green" style="float:left; margin-left:5px">Lihat</a>
                            <span style="float:left; margin-left:5px">
                          <form class="" action="{{ route('admin.destinasi.delete', $value->slug) }}" method="post">
                              {{ csrf_field() }}
                              {{ method_field('DELETE') }}
                              <button type="submit" class="btn red">Hapus</button>
                          </form>
                        </span>
                        </td>
                    </tr>
                @endforeach
            </table>
            {{ $destination->links('vendor.pagination.paging') }}
        </div>
    </div>
    <!-- The Modal -->
    <div class="modal" id="modal">
        <div class="modalDialog">
            <section class="modalContent">
                <header class="modalHeader">
                    <h2 class="modalTitle">Edit Status</h2>
                    <a href="#" id="modalClose" class="modalClose">&times;</a>
                </header>
                <div class="modalBody">
                    <div class="modalText">
                        <form id="form-status" action="#" method="post" style="text-align: center">
                            {{ csrf_field() }}
                            <div>
                                <label for="status" style="margin-right: 1rem">Status</label>
                                <select name="status" id="status" style="color: #151515; margin-bottom: 1rem">
                                    <option value="published">Publish</option>
                                    <option value="pending">Pending</option>
                                </select>
                            </div>
                            <div>
                                <button type="submit" class="btn green">Update</button>
                            </div>
                        </form>
                    </div>
                </div>
            </section>
        </div>
    </div>
@endsection
@section('scripts')
    <script type="text/javascript">
        $(document).ready(function () {
            var slug = null;

            $('button#status').click(function (e) {
                e.preventDefault();
                slug = $(this).attr('value');

                var str = "{{ route('admin.destinasi.status', 'slug') }}";
                var url = str.replace("slug", slug);

                $('form#form-status').attr('action', url);
                $('#modal').css('display', 'block');
            });
            $('a#modalClose').click(function (e) {
                e.preventDefault();
                $('#modal').css('display', 'none');
            });
        });
    </script>
@endsection
