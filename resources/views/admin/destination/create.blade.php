@extends('admin.layouts.app')
@section('content')
    <div class="container" style="float:left">
        <div class="content" style="float:left; width:100%; padding: 0px 4px 20px 0px;">
            <h2 style="margin-left:22px">Buat Destinasi</h2>
            @foreach($errors->all() as $error)
                <div class="notification">
                    <div class="alert alert-danger">
                        {{ $error }}
                    </div>
                </div>
            @endforeach
            <form class="form" action="{{ Route('admin.destinasi.store') }}" method="post"
                  enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="group">
                    <label for="cover.0">Foto Sampul <span class="required">*</span></label>
                    <input id="cover.0" class="input-field input-field-long" type="file" name="cover[]" required>
                    <label for="title">Judul <span class="required">*</span></label>
                    <input id="title" class="input-field input-field-long" type="text" name="title" required>
                </div>
                <div class="group">
                    <label for="location">Lokasi <span class="required">*</span></label>
                    <input id="location" class="input-field input-field-long" type="text" name="location">
                </div>
                <div class="group">
                    <label for="images.0">Gambar <span class="required">*</span></label>
                    <input id="images.0" class="input-field input-field-long" type="file" name="images[]" multiple required>
                </div>
                <div class="group-full">
                  <label for="description">Deskripsi <span class="required">*</span></label>
                  <textarea name="description" id="description" class="input-field input-field-long"
                            required></textarea>
                </div>
                <div class="group-full" style="text-align: center; margin-top:12px;">
                    <button class="btn green" type="submit">Simpan</button>
                </div>
            </form>
        </div>
    </div>
@endsection
