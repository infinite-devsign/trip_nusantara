<?php
/**
 * Created by PhpStorm.
 * User: ANDROMEDA
 * Date: 4/3/2018
 * Time: 8:32 PM
 */
?>
@extends('admin.layouts.app')
@section('content')
    <div class="container">
        <div class="content" style="float:left; padding: 0; width:100%">
            <h2 style="margin-left:22px">Edit Galeri</h2>
            @foreach($errors->all() as $error)
                <div class="notification">
                    <div class="alert alert-danger">
                        {{ $error }}
                    </div>
                </div>
            @endforeach
            <form class="form" action="{{ Route('admin.galeri.update', $gallery->slug) }}" method="post" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="group">
                    <label for="title">Judul <span class="required">*</span></label>
                    <input id="title" class="input-field input-field-long" type="text" name="title"
                           value="{{ $gallery->title }}" required>
                    <label for="description">Deskripsi <span class="required">*</span></label>
                    <textarea name="description" id="description" class="input-field input-field-long"
                              required>{{ $gallery->description }}</textarea>
                </div>
                <div class="group">
                    <button class="btn green" type="submit" style="text-align: center">Save</button>
                </div>
            </form>
        </div>
    </div>
@endsection
