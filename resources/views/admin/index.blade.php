@extends('admin.layouts.app')
@section('header')
@endsection
@section('content')
    <div class="container">
        @if (session('status'))
            <div class="notification">
                <div class="alert alert-success">
                    {{ session('status') }}
                </div>
            </div>
        @endif
        <div class="count-box">
            <div class="top">
                {{ $destination }}
            </div>
            <div class="bottom blue">
                Destinasi
                <a href="{{ route('admin.destinasi.create') }}" class="btn white">Tambah</a>
                <a href="{{ route('admin.destinasi') }}" class="btn white">List</a>
            </div>
        </div>
        <div class="count-box">
            <div class="top">
                {{ $gallery }}
            </div>
            <div class="bottom green">
                Galeri
                <a href="{{ route('admin.galeri.create') }}" class="btn white">Tambah</a>
                <a href="{{ route('admin.galeri') }}" class="btn white">List</a>
            </div>
        </div>
        <div class="count-box">
            <div class="top">
                {{ $culinary }}
            </div>
            <div class="bottom red">
                Kuliner
                <a href="{{ route('admin.kuliner.create') }}" class="btn white">Tambah</a>
                <a href="{{ route('admin.kuliner') }}" class="btn white">List</a>
            </div>
        </div>
        <div class="count-box">
            <div class="top">
                {{ $event }}
            </div>
            <div class="bottom red">
                Event
                <a href="{{ route('admin.event.create') }}" class="btn white">Tambah</a>
                <a href="{{ route('admin.event') }}" class="btn white">List</a>
            </div>
        </div>
    </div>
@endsection
