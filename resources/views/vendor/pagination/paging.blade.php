<?php
/**
 * Created by PhpStorm.
 * User: ANDROMEDA
 * Date: 3/25/2018
 * Time: 1:30 PM
 */
?>
@if ($paginator->hasPages())
    <div class="center">
        <div class="pagination">
            @if ($paginator->onFirstPage())
                <a href="#" class="disabled">&laquo;</a>
            @else
                <a href="{{ $paginator ->previousPageUrl() }}">&laquo;</a>
            @endif

            {{-- Pagination Elements --}}
            @foreach($elements as $element)
                {{-- "Three Dots" Separator --}}
                @if (is_string($element))
                    <a href="#" class="disabled">{{ $element }}</a>
                @endif

                @if (is_array($element))
                    @foreach($element as $page => $url)
                        @if ($page == $paginator->currentPage())
                            <a class="blue" href="#">{{ $page }}</a>
                        @else
                            <a href="{{ $url }}">{{ $page }}</a>
                        @endif
                    @endforeach
                @endif
            @endforeach

            @if ($paginator->hasMorePages())
                <a href="{{ $paginator->nextPageUrl() }}">&raquo;</a>
            @else
                <a href="#" class="disabled">&raquo;</a>
            @endif

        </div>
    </div>
@endif