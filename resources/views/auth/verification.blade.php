<?php
/**
 * Created by PhpStorm.
 * User: ANDROMEDA
 * Date: 3/21/2018
 * Time: 2:48 PM
 */
?>
@extends('layouts.app')
@section('header')
  <style>
    body {
      background: #eee;
    }
    .register-success {
      margin: 10px auto 100px auto;
      width: 400px;
      border: 3px solid #f1f1f1;
      padding: 10px;
      background: #fff;
      text-align: center;
    }
  </style>
@endsection

@section('content')
<div class="container">
  <div class="register-success">
    <h1>Registrasi Berhasil</h1>
    <p>Silahkan cek email anda untuk konfirmasi :D</p>
  </div>
</div>
@endsection
