<?php
/**
 * Created by PhpStorm.
 * User: ANDROMEDA
 * Date: 3/23/2018
 * Time: 8:59 PM
 */
?>
@extends('layouts.app')

@section('content')

<div class=”container”>

    <div class=”row”>

        <div class=”col-md-8">

        <div class=”panel”>

            <div class=”panel-heading”>Password berhasil dirubah</div>

            <div class=”panel-body”>

                Password anda telah dirubah. Klik link disamping untuk <a href=”{{url('/login')}}”>login</a>

            </div>

        </div>

    </div>

</div>

</div>

@endsection

