@extends('layouts.app')
@section('header')
  <style>
    body {
      background: #eee;
    }
    .login {
      margin: 100px auto;
      width: 300px;
      border: 3px solid #f1f1f1;
      padding: 10px;
      background: #fff;
    }
    .login h1 {
      text-align: center;
    }
    .login a {
      text-decoration: none;
      color: green;
    }
    .text {
      margin-left: 12px;
    }
    .btn {
      margin-top: 12px;
    }
    strong {
      color: red;
    }
  </style>
@endsection

@section('content')
{{-- <div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Login</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('login') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="email" class="col-sm-4 col-form-label text-md-right">E-Mail Address</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus>

                                @if ($errors->has('email'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">Password</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-6 offset-md-4">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> Remember Me
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-8 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    Login
                                </button>

                                <a class="btn btn-link" href="{{ route('password.request') }}">
                                    Forgot Your Password?
                                </a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div> --}}
<div class="container">
  <div class="login">
    <h1>Masuk</h1>
    <form class="form" method="POST" action="{{ route('login') }}">
      @csrf
      <label for="email">Email</label>
      <input id="email" class="input-field input-field-long {{ $errors->has('password') ? ' is-invalid' : '' }}" type="email" name="email" value="{{ old('email') }}" required autofocus>
      @if ($errors->has('email'))
          <span class="invalid-feedback">
              <strong>{{ $errors->first('email') }}</strong>
          </span>
      @endif
      <label for="password">Password</label>
      <input id="password" class="input-field input-field-long {{ $errors->has('password') ? ' is-invalid' : '' }}" type="password" name="password" value="{{ old('password') }}" required>
      @if ($errors->has('password'))
          <span class="invalid-feedback">
              <strong>{{ $errors->first('password') }}</strong>
          </span>
      @endif
      <button class="btn green" type="submit">Submit</button>
    </form>
    <div style="margin: 12px;">
      <a href="{{ route('password.request') }}">Lupa Password ?</a>
    </div>
    <span class="text">Belum punya akun ?</span> <a href="{{ route("register") }}">Daftar</a>
  </div>
</div>
@endsection
