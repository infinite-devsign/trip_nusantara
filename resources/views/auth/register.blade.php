@extends('layouts.app')
@section('header')
  <style>
    body {
      background: #eee;
    }
    .login {
      margin: 10px auto 30px auto;
      width: 300px;
      border: 3px solid #f1f1f1;
      padding: 10px;
      background: #fff;
    }
    .login h1 {
      text-align: center;
    }
    .login a {
      text-decoration: none;
      color: green;
    }
    .text {
      margin-left: 12px;
    }
    .btn {
      margin-top: 12px;
    }
    strong {
      color: red;
    }
  </style>
@endsection

@section('content')
{{-- <div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Register</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('register') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">Name</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}" required autofocus>

                                @if ($errors->has('name'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">E-Mail Address</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>

                                @if ($errors->has('email'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">Password</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password-confirm" class="col-md-4 col-form-label text-md-right">Confirm Password</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    Register
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div> --}}
<div class="container">
  <div class="login">
    <h1>Daftar</h1>
    <form class="form" method="POST" action="{{ route('register') }}">
      @csrf
      <label for="name">Nama</label>
      <input id="name" class="input-field input-field-long {{ $errors->has('name') ? ' is-invalid' : '' }}" type="name" name="name" value="{{ old('name') }}" required autofocus>
      @if ($errors->has('name'))
          <span class="invalid-feedback">
              <strong>{{ $errors->first('name') }}</strong>
          </span>
      @endif
      <label for="username">Username</label>
      <input id="username" class="input-field input-field-long {{ $errors->has('username') ? ' is-invalid' : '' }}" type="username" name="username" value="{{ old('username') }}" required>
      @if ($errors->has('username'))
          <span class="invalid-feedback">
              <strong>{{ $errors->first('username') }}</strong>
          </span>
      @endif
      <label for="email">Email</label>
      <input id="email" class="input-field input-field-long {{ $errors->has('email') ? ' is-invalid' : '' }}" type="email" name="email" value="{{ old('email') }}" required>
      @if ($errors->has('email'))
          <span class="invalid-feedback">
              <strong>{{ $errors->first('email') }}</strong>
          </span>
      @endif
      <label for="password">Password</label>
      <input id="password" class="input-field input-field-long {{ $errors->has('password') ? ' is-invalid' : '' }}" type="password" name="password" value="{{ old('password') }}" required>
      @if ($errors->has('password'))
          <span class="invalid-feedback">
              <strong>{{ $errors->first('password') }}</strong>
          </span>
      @endif
      <label for="password-confirm">Konfirmasi Password</label>
      <input id="password-confirm" class="input-field input-field-long {{ $errors->has('password') ? ' is-invalid' : '' }}" type="password" name="password_confirmation" value="{{ old('password-confirm') }}" required>
      @if ($errors->has('password-confirm'))
          <span class="invalid-feedback">
              <strong>{{ $errors->first('password-confirm') }}</strong>
          </span>
      @endif
      <button class="btn green" type="submit">Submit</button>
    </form>
    <span class="text">Sudah punya akun ?</span> <a href="{{ route("login") }}">Masuk</a>
  </div>
</div>
@endsection
