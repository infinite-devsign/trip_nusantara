<?php
/**
 * Created by PhpStorm.
 * User: ANDROMEDA
 * Date: 3/21/2018
 * Time: 2:50 PM
 */
?>
@extends('layouts.app')

@section('content')

    <div class=”container”>

        <div class=”row”>

            <div class=”col-md-8 col-md-offset-2">

            <div class=”panel panel-default”>

                <div class=”panel-heading”>Registration Confirmed</div>

                <div class=”panel-body”>

                    Email anda telah di verifikasi. Klik link disamping untuk <a href=”{{url('/login')}}”>login</a>

                </div>

            </div>

        </div>

    </div>

    </div>

@endsection
